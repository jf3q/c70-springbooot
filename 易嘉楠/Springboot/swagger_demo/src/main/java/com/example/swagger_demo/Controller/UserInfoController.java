package com.example.swagger_demo.Controller;


import com.example.swagger_demo.entity.UserInfo;
import com.example.swagger_demo.service.UserInfoService;
import com.example.swagger_demo.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
@Api(tags = "User操作接口")
public class UserInfoController {
    @Autowired
    UserInfoService userInfoService;
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查找用户对象", notes = "根据id查找用户对象")
    public ResultVo<UserInfo> getUser(@ApiParam(value = "用户ID", example = "1", required = true)@PathVariable Integer id){

        UserInfo userInfo=  userInfoService.getUser(id);
        return ResultVo.success("",userInfo);
    }

    @GetMapping
    @ApiOperation(value = "获取所有用户对象", notes = "获取所有用户，无需参数")
    public ResultVo<List<UserInfo>> getList(){
        List<UserInfo> list=userInfoService.getList();
        return ResultVo.success("",list);
    }
}
