package com.example.swagger_demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    public String code;
    public String mess;
    public T data;

    public static<T> ResultVo<T> success(String mess,T data){
        return new ResultVo<T>("200",mess,data);
    }
    public static<T> ResultVo<T> error(String mess){
        return new ResultVo<T>("500",mess,null);
    }


}
