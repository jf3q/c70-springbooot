package com.demo1.vo;

public class Resultvo {
    private String code;
    private String message;
    private Object object;


    public Resultvo(){}

    public Resultvo(String code,String message,Object object){
        this.code = code;
        this.message=message;
        this.object = object;
    }


    public Resultvo sussue(String message,Object object){
        return new Resultvo("2000",message,object);
    }

    public Resultvo error(String message){
        return new Resultvo("5000",message,"");
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
