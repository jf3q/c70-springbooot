package com.demo1.service;

import com.demo1.entity.UserInfo;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserInfoService {
    public UserInfo getUser(Integer id) {
        return new UserInfo(1,"张三","123");
    }

    public List<UserInfo> getList() {
        List<UserInfo>list= Arrays.asList(
                new UserInfo[]{
                        new UserInfo(1,"李四","123"),
                        new UserInfo(2,"王五","124"),
                        new UserInfo(3,"刘六","125")
                }
        );
        return list;
    }
}
