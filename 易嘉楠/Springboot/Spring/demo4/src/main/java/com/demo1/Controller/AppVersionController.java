package com.demo1.Controller;

import com.demo1.entity.AppInfo;
import com.demo1.entity.AppVersion;
import com.demo1.entity.DevUser;
import com.demo1.service.AppInfoservice;
import com.demo1.service.AppVersionservice;
import com.demo1.ui.Session;
import com.demo1.vo.Resultvo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RequestMapping("/Appver")
@RestController

public class AppVersionController {
   @Autowired
    private AppVersionservice appVersionservice;
   @Autowired
    private AppInfoservice appInfoservice;

    @RequestMapping("/ver")
    @ResponseBody
    public Resultvo page(int id){
        Resultvo resultvo = new Resultvo();
        List<AppVersion> appVersions = appVersionservice.queryById(id);
        return resultvo.sussue("",appVersions);
    }


    @RequestMapping("/insert")
    @ResponseBody
    public Resultvo add(AppVersion appVersion, MultipartFile log, HttpServletRequest request){
        Resultvo resultvo = new Resultvo();
        String token = request.getHeader("token");
        DevUser devUser = (DevUser) Session.GETmap(token);
        if (log!=null){
            String name = log.getOriginalFilename();
            String suiffx = name.substring(name.lastIndexOf("."),name.length());
            if (log.getSize()>1024*500){
                return resultvo.error("文件大于500kb");
            }
            else if (suiffx.equalsIgnoreCase(".apk")){
                String realth = request.getServletContext().getRealPath("/file/appver");
                File file = new File(realth);
                if (!file.exists()){
                    file.mkdirs();
                }
                String newname = UUID.randomUUID().toString()+suiffx;
                File file1 = new File(realth+"/"+newname);
                try {
                    log.transferTo(file1);
                    appVersion.setDownloadlink("/file/appver/"+newname);
                    appVersion.setApklocpath(file1.toString());
                    appVersion.setApkfilename(newname);
                } catch (IOException e) {
                    return resultvo.error("文件上传失败");
                }
            }
            else {
                return resultvo.error("文件格式不正确");
            }
        }
        appVersion.setCreatedby(devUser.getCreatedby());
        appVersion.setCreationdate(new Date());
        appVersion.setPublishstatus(3L);
        int i = appVersionservice.insert(appVersion);
        if (i!=0){
            AppInfo appInfo = new AppInfo();
            appInfo.setStatus(1L);
            appInfo.setId(appVersion.getAppid());
            appInfo.setVersionid(appVersion.getId());
            appInfoservice.update(appInfo);
            return resultvo.sussue("上传成功",i);
        }
        else if (i==0){
            return resultvo.error("上传失败");
        }

        return null;
    }

}
