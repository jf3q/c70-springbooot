package com.example.bootmybaties.controller;

import com.example.bootmybaties.entity.Book;
import com.example.bootmybaties.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;
    @GetMapping
    public String geAll(Model model){
       List<Book>list= bookService.getAll();
       model.addAttribute("books",list);
       return "BookList";
    }
}
