package com.example.bootmybaties.service;

import com.example.bootmybaties.entity.Book;
import com.example.bootmybaties.mapper.BookMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    BookMapper bookMapper;
    public List<Book>getAll(){
        return bookMapper.findAllBooks();
    }
}
