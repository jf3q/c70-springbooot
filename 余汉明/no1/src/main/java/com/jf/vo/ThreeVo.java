package com.jf.vo;

import java.util.ArrayList;
import java.util.List;

public class ThreeVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<ThreeVo> list=new ArrayList<>();

    public List<ThreeVo> getList() {
        return list;
    }

    public void setList(List<ThreeVo> list) {
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }
}
