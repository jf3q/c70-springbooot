package com.jf.dao;

import com.jf.entity.U;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 手游类别(U)表数据库访问层
 *
 * @author makejava
 * @since 2023-11-28 13:37:45
 */
public interface UDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    U queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param u        查询条件
     * @return 对象列表
     */
    List<U> queryAllBy(U u);

    /**
     * 统计总行数
     *
     * @param u 查询条件
     * @return 总行数
     */
    long count(U u);

    /**
     * 新增数据
     *
     * @param u 实例对象
     * @return 影响行数
     */
    int insert(U u);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<U> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<U> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<U> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<U> entities);

    /**
     * 修改数据
     *
     * @param u 实例对象
     * @return 影响行数
     */
    int update(U u);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

