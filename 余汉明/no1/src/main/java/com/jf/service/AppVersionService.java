package com.jf.service;

import com.jf.controller.R;
import com.jf.entity.AppVersion;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AppVersionService {
    List<AppVersion> getList(int appid);
    R addVersion(AppVersion appVersion, MultipartFile apkFIle, HttpServletRequest request);
}
