package com.jf.service;

import com.jf.entity.U;
import com.jf.dao.UDao;
import com.jf.vo.ThreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService  {
    @Autowired
    UDao uDao;
    public ThreeVo getCategory(){
        List<ThreeVo> voList=new ArrayList<>();
        //1。查询所有得数据  把所有的信息赋给Vo
        for (U u : uDao.queryAllBy(new U())) {
            ThreeVo threeVo=new ThreeVo();
            BeanUtils.copyProperties(u,threeVo);
            voList.add(threeVo);
        }
        //2.改变接口 变成树形接口
        ThreeVo threeVo=new ThreeVo();
        //把所有Vo信息遍历出来
        for (ThreeVo vo : voList) {
            if (vo.getParentid()==null){  //如果等于null 也是说他就是全部游戏
                //为这个全部游戏装填一级分类
                threeVo=findChildren(vo,voList);
            }
        }
        return threeVo;
    }

    //通过递归的方式拿值
    private ThreeVo findChildren(ThreeVo vo, List<ThreeVo> voList) {
        for (ThreeVo threeVo : voList) {
            if (vo.getId()== threeVo.getParentid()){
                vo.getList().add(threeVo);
                findChildren(threeVo,voList);
            }
        }
        return vo;
    }
}
