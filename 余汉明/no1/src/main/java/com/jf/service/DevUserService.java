package com.jf.service;

import com.jf.dto.LoginUserDto;
import com.jf.vo.LoginUser;

public interface DevUserService {
    LoginUser SelectUser(LoginUserDto userDto);
}
