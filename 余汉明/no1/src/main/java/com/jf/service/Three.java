package com.jf.service;

import com.jf.dao.UDao;
import com.jf.entity.U;
import com.jf.vo.ThreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Three {
    @Autowired
    UDao uDao;
    public ThreeVo getThree(){
        List<ThreeVo> threeVoList=new ArrayList<>();
        for (U u : uDao.queryAllBy(new U())) {
            ThreeVo threeVo=new ThreeVo();
            BeanUtils.copyProperties(u,threeVo);
            threeVoList.add(threeVo);
        }

        ThreeVo threeVo=new ThreeVo();
        for (ThreeVo vo : threeVoList) {
            if (vo.getParentid()==null){
                threeVo=findChildren(vo,threeVoList);
            }
        }
        return threeVo;
    }

    private ThreeVo findChildren(ThreeVo vo, List<ThreeVo> threeVoList) {
        for (ThreeVo threeVo : threeVoList) {
            if (vo.getId()==threeVo.getParentid()){
                vo.getList().add(threeVo);
                findChildren(threeVo,threeVoList);
            }
        }
        return vo;
    }
}
