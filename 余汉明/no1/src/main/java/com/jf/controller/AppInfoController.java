package com.jf.controller;

import com.github.pagehelper.PageInfo;
import com.jf.dto.AppInfoDto;
import com.jf.entity.AppInfo;
import com.jf.entity.AppVersion;
import com.jf.service.AppInfoService;
import com.jf.service.AppVersionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
public class AppInfoController {
    @Autowired
    AppInfoService appInfoService;
    //AppInfoService类
    @Autowired
    AppVersionService appVersionService;
    @RequestMapping("/getPage")
    public R getPage(@RequestBody AppInfoDto appInfoDto,
                     @RequestParam(defaultValue = "1") Integer pageNum,@RequestParam(defaultValue = "5") Integer pageSize
                        ,HttpServletRequest request ){
        String token = request.getHeader("Token");
        PageInfo<AppInfo> pageInfo=appInfoService.getPage(appInfoDto,pageNum,pageSize,token);
        return R.success("查询成功！",pageInfo);
    }
    @PostMapping("/addApp")
    public R addApp( AppInfo appInfo, MultipartFile logFile, HttpServletRequest request){
        String token = request.getHeader("Token");
        return appInfoService.updateAddApp(appInfo,logFile,request);
    }
    @RequestMapping("/selectApk")
    public R selectApk(String apkname){
        AppInfo appInfo = appInfoService.selectApknameInt(apkname);
        if (appInfo==null){
            return R.success("apk名称可以使用", true);
        }else {
            return R.error("apk名称已存在");
        }
    }
    @RequestMapping("/deleteApp")
    public R deleteApp(Integer id,HttpServletRequest request){
        if (appInfoService.deleteApp(id,request)>0){
            return R.success("删除成功",null);
        }else {
            return R.error("删除失败");
        }
    }

    @RequestMapping("/selectById")
    public R selectApp(Integer id){
        AppInfo appInfo = appInfoService.selectById(id);
        AppInfoDto appInfoDto=new AppInfoDto();
        BeanUtils.copyProperties(appInfo,appInfoDto);
        return R.success("查询成功",appInfoDto);
    }
    //查询版本
    @RequestMapping("/getAppVersion")
    public R getAppVersion(Integer id){
        List<AppVersion> list = appVersionService.getList(id);
        return R.success("查询成功",list);
    }
    //apk上传
    @PostMapping("/addVersion")
    public R addVersion( AppVersion appVersion, MultipartFile apkFile, HttpServletRequest request){
        return appVersionService.addVersion(appVersion,apkFile,request);
    }
    //上架 下架
    @RequestMapping("/sale")
    public R sale(Integer id){
        AppInfo appInfo = appInfoService.selectById(id);
        if (appInfo.getStatus()==4L){
            appInfo.setStatus(5L);
        }else if (appInfo.getStatus()==5L){
            appInfo.setStatus(4L);
        }
        appInfo.setOffsaledate(new Date());
        int update = appInfoService.update(appInfo);
        return  R.success("成功",update);
    }
    //审核通过 和不通过
    @RequestMapping("/auditor")
    public R auditor(AppInfo appInfo){
        appInfoService.update(appInfo);
        return R.success("成功",null);
    }
}



