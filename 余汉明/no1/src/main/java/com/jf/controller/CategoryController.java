package com.jf.controller;

import com.jf.service.Three;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {
    @Autowired
    Three categoryService;

    @RequestMapping("/three")
    public R getThree(){
        return R.success("成功",categoryService.getThree());
    }
}
