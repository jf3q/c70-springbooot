package com.jf.controller;


public class R {
    private Integer code; //编码：2000成功 5000失败

    private String msg; //错误信息

    private Object data; //数据
    public static   R error(String msg){
        return new R(5000,msg,null);
    }
    public static R success(String msg,Object obj){
        return new R(2000,msg,obj);
    }

    public R(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public R() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
