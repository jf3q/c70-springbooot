package com.jf.dto;


public class AppInfoDto {
    /**
     * 软件名称
     */
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    private String apkname;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    private Long status;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private Long flatformid;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private Long categorylevel1;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private Long categorylevel2;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private Long categorylevel3;
    /**
     * 支持ROM
     */
    private String supportrom;
    /**
     * 界面语言
     */
    private String interfacelanguage;
    /**
     * 软件大小（单位：M）
     */
    private Double softwaresize;

    private String appinfo;
    private String logopicpath;

    public String getLogopicpath() {
        return logopicpath;
    }

    public void setLogopicpath(String logopicpath) {
        this.logopicpath = logopicpath;
    }

    public String getSupportrom() {
        return supportrom;
    }

    public void setSupportrom(String supportrom) {
        this.supportrom = supportrom;
    }

    public String getInterfacelanguage() {
        return interfacelanguage;
    }

    public void setInterfacelanguage(String interfacelanguage) {
        this.interfacelanguage = interfacelanguage;
    }

    public Double getSoftwaresize() {
        return softwaresize;
    }

    public void setSoftwaresize(Double softwaresize) {
        this.softwaresize = softwaresize;
    }

    public String getAppinfo() {
        return appinfo;
    }

    public void setAppinfo(String appinfo) {
        this.appinfo = appinfo;
    }

    public String getSoftwarename() {
        return softwarename;
    }

    public void setSoftwarename(String softwarename) {
        this.softwarename = softwarename;
    }

    public String getApkname() {
        return apkname;
    }

    public void setApkname(String apkname) {
        this.apkname = apkname;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getFlatformid() {
        return flatformid;
    }

    public void setFlatformid(Long flatformid) {
        this.flatformid = flatformid;
    }

    public Long getCategorylevel1() {
        return categorylevel1;
    }

    public void setCategorylevel1(Long categorylevel) {
        this.categorylevel1 = categorylevel;
    }

    public Long getCategorylevel2() {
        return categorylevel2;
    }

    public void setCategorylevel2(Long categorylevel2) {
        this.categorylevel2 = categorylevel2;
    }

    public Long getCategorylevel3() {
        return categorylevel3;
    }

    public void setCategorylevel3(Long categorylevel3) {
        this.categorylevel3 = categorylevel3;
    }


}
