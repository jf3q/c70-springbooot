package com.bdqn.nucleicacid.dao;

import com.bdqn.nucleicacid.pojo.Hospital;

import java.util.List;

public interface HospitalDao {
    /**
     * 核酸检测机构
     * @return
     */
    List<Hospital> listHospital();
}
