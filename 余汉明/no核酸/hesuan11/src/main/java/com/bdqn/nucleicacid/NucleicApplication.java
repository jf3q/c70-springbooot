package com.bdqn.nucleicacid;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.bdqn.nucleicacid.dao")
public class NucleicApplication {

    public static void main(String[] args) {
        SpringApplication.run(NucleicApplication.class, args);
    }

}
