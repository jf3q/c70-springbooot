package com.bdqn.nucleicacid.service;

import com.bdqn.nucleicacid.dao.HospitalDao;
import com.bdqn.nucleicacid.pojo.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService{
    @Autowired
    private HospitalDao hospitalDao;

    @Override
    public List<Hospital> listHospital() {
        return hospitalDao.listHospital();
    }
}
