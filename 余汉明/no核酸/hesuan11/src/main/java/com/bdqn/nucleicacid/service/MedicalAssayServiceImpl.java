package com.bdqn.nucleicacid.service;

import com.bdqn.nucleicacid.dao.MedicalAssayDao;
import com.bdqn.nucleicacid.pojo.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayServiceImpl implements MedicalAssayService{
    @Autowired
    private MedicalAssayDao medicalAssayDao;
    @Override
    public List<MedicalAssay> listMedical(Integer hospitalId) {
        return medicalAssayDao.listMedical(hospitalId);
    }

    @Override
    public int updateMedical(MedicalAssay medicalAssay) {
        MedicalAssay medicalAssay1 = new MedicalAssay().setAssayResult(1).setId(medicalAssay.getId());
        return medicalAssayDao.updateMedical(medicalAssay1);
    }

    @Override
    public int addMedical(MedicalAssay medicalAssay) {
        return medicalAssayDao.addMedical(medicalAssay);
    }
}
