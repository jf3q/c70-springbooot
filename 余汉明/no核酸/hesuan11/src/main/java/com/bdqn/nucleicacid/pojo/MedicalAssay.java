package com.bdqn.nucleicacid.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MedicalAssay {

  private Integer id;
  private String assayUser;
  private Integer hospitalId;
  private Integer assayResult;
  private String phone;
  private String cardNum;
  @JsonFormat(pattern = "yyyy-MM-dd")
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date assayTime;
  private Hospital hospital;

}
