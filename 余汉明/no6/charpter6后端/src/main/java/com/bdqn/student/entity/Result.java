package com.bdqn.student.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据统一返回
 * @param <T>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    private String code;
    private String message;
    private T data;

    /**
     * 成功提示
     */
    public static<T> Result<T> success(String message,T data){
        return new Result<T>("2000",message,data);
    }
    /**
     * 失败提示
     */
    public static<T> Result<T> error(String message){
        return new Result<T>("5000",message,null);
    }
}
