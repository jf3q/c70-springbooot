package com.bdqn.student.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bdqn.student.entity.Student;

public interface IStudentService extends IService<Student> {

    IPage<Student> getPage(String studentname,Integer pageNum);
}
