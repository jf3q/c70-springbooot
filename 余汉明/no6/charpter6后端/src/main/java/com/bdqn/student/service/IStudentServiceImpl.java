package com.bdqn.student.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bdqn.student.entity.Student;
import com.bdqn.student.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IStudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

    @Autowired
    private StudentMapper studentMapper;

    /**
     * 分页带条件查询
     * @param studentname
     * @param pageNum
     * @return
     */
    @Override
    public IPage<Student> getPage(String studentname, Integer pageNum) {
        LambdaQueryWrapper<Student> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(Student::getStudentname,studentname);
        IPage<Student> page = new Page<>(pageNum,3);
        page=studentMapper.selectPage(page, lambdaQueryWrapper);
        return page;
    }
}
