package com.bdqn.student.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdqn.student.entity.Result;
import com.bdqn.student.entity.Student;
import com.bdqn.student.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private IStudentService iStudentService;

    /**
     * 分页带条件查询
     * @return
     */
    @GetMapping("")
    public Result findAllStudent(@RequestParam(name = "studentname",defaultValue ="")String studentname, @RequestParam(defaultValue = "1")Integer pageNum){
        IPage<Student> page = iStudentService.getPage(studentname, pageNum);
        return Result.success("查询成功",page);
    }

    /**
     * 新增或修改
     * @param
     * @return
     */
    @PostMapping("")
    public Result save(@RequestBody Student student){
        iStudentService.saveOrUpdate(student);
        return Result.success("操作成功",null);
    }
    /**
     * 删除
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        iStudentService.removeById(id);
        return Result.success("删除成功",null);
    }
}
