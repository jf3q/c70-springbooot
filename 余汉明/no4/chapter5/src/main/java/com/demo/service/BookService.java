package com.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.dao.BookDao;
import com.demo.entity.Book;

import java.util.List;

public interface BookService extends IService<Book> {
    int deleteBatchIds(List<Integer> id);
}
