package com.demo1.service;


import com.demo1.entity.AppInfo;
import com.demo1.entity.page.Appinfopage;
import org.apache.ibatis.annotations.Param;

public interface AppInfoservice {
    Appinfopage page(AppInfo appInfo, int pagenum, int pagesize);
    int insert(AppInfo appInfo);
    int update(AppInfo appInfo);
    int apk(String apkName);
    int deleteById(int id);
    AppInfo queryById(int id);
}
