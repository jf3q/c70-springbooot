package com.demo1.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


@Configuration
@EnableOpenApi
public class SwaggerConfig {
    @Bean
    public Docket desertsApi(Environment environment){
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.demo1.Controller"))//按包扫描,
                .paths(PathSelectors.any())
                .build()
                .groupName("jf3q")
                .enable(true);
    }

    //
    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("用户管理系统说明API文档")//标题
                .description("用户管理系统说明API文档")//描述
                .contact(new Contact("jfit", "https://www.jf3q.com", "12345@qq.com"))//作者信息
//                .termsOfServiceUrl("https://www.sike.com")
                .version("1.0")//版本号
                .build();
    }
}
