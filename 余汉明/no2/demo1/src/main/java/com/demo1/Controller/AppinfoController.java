package com.demo1.Controller;

import com.demo1.entity.AppCategory;
import com.demo1.entity.AppInfo;
import com.demo1.entity.AppVersion;
import com.demo1.entity.DevUser;
import com.demo1.entity.page.Appinfopage;
import com.demo1.service.AppCategoryservice;
import com.demo1.service.AppInfoservice;
import com.demo1.service.AppVersionservice;
import com.demo1.ui.Session;
import com.demo1.vo.Resultvo;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/appinfo")
public class AppinfoController {


    @Value("${web.upload-path}")
    private String uploadpath;

    @Autowired
    private AppInfoservice appInfoservice ;
    @Autowired
    private AppCategoryservice appCategoryservice;
    @Autowired
    private AppVersionservice appVersionservice;


    @RequestMapping("/page")
    @ResponseBody
    public Resultvo page(@RequestBody AppInfo appInfo, int pagenum, HttpServletRequest request){
        Resultvo resultvo = new Resultvo();
//        String token = request.getHeader("token");
//        String [] sprit = token.split("-");
//        if (sprit[1].equals("admin")){
//            appInfo.setStatus(1L);
//        }
        Appinfopage appinfopage = appInfoservice.page(appInfo,pagenum,5);
        return resultvo.sussue("",appinfopage);
    }


    @RequestMapping(value = "/roke/{id}",method = RequestMethod.POST)
    @ResponseBody
    public Resultvo roke(@PathVariable int id){
        Resultvo resultvo = new Resultvo();
        List<AppCategory> appInfo = appCategoryservice.queryById(id);
        return resultvo.sussue("",appInfo);
    }

    @RequestMapping("/insert")
    @ResponseBody
    public Resultvo add(AppInfo appInfo, MultipartFile log, HttpServletRequest request){
        System.out.println("图片真假1sss");
        Resultvo resultvo = new Resultvo();
        String token = request.getHeader("token");
        DevUser devUser = (DevUser) Session.GETmap(token);
        if (log!=null){
            String name = log.getOriginalFilename();
            String suiffx = name.substring(name.lastIndexOf("."),name.length());
            if (log.getSize()>1024*500){
                return resultvo.error("文件大于500kb");
            }
            else if (suiffx.equalsIgnoreCase(".jpg")||suiffx.equalsIgnoreCase(".peg")||suiffx.equalsIgnoreCase(".gif")){
//                String realth = request.getServletContext().getRealPath("/file/appinfo/log");
//                File file = new File(realth);
//                if (!file.exists()){
//                    file.mkdirs();
//                }
                String newname = UUID.randomUUID().toString()+suiffx;
                File file1 = new File(uploadpath+"/"+newname);
                try {
                    log.transferTo(file1);
                    appInfo.setLogopicpath("/"+newname);
                    appInfo.setLogolocpath(file1.toString());
                } catch (IOException e) {
                    return resultvo.error("文件上传失败");
                }
            }
            else {
                return resultvo.error("文件格式不正确");
            }
        }
        if (appInfo.getId()==null){
            appInfo.setStatus(1L);
            appInfo.setDevid(devUser.getId());
            appInfo.setCreatedby(devUser.getCreatedby());
            appInfo.setCreationdate(new Date());
            int i = appInfoservice.insert(appInfo);
            if (i!=0){
                return resultvo.sussue("新增成功",i);
            }
            else if (i==0){
                return resultvo.error("新增失败");
            }
        }
        else if (appInfo.getId()!=0){
            appInfo.setModifyby(devUser.getId());
            appInfo.setModifydate(new Date());
            appInfo.setStatus(1L);
            appInfo.setUpdatedate(new Date());
            int i = appInfoservice.update(appInfo);
            if (i!=0){
                return resultvo.sussue("修改成功",i);
            }
            else if (i==0){
                return resultvo.error("修改失败");
            }
        }
        return null;
    }

    @RequestMapping("/apk")
    @ResponseBody
    public Resultvo apk(String  apkName){
        Resultvo resultvo = new Resultvo();
        int i = appInfoservice.apk(apkName);
        return resultvo.sussue("",i);
    }


    @RequestMapping("/del")
    @ResponseBody
    public Resultvo del(int id,HttpServletRequest request){
        AppInfo appInfo = appInfoservice.queryById(id);
        Resultvo resultvo = new Resultvo();
        int i = appInfoservice.deleteById(id);
        List<AppVersion> appVersions = appVersionservice.queryById(id);

        for (AppVersion appVersion : appVersions) {
            String realpath = request.getServletContext().getRealPath(appVersion.getDownloadlink());
            File file = new File(realpath);
            if (file.exists()){
                file.delete();
            }
        }
        appVersionservice.deleteById(id);

        String realpath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
        File file1 = new File(realpath);
        if (file1.exists()){
            file1.delete();
        }
        return resultvo.sussue("",i);

    }

    @RequestMapping("/up")
    @ResponseBody
    public Resultvo up(int id,int type){
        Resultvo resultvo = new Resultvo();
        AppInfo appInfo = new AppInfo();
        appInfo.setId((long) id);
       if (type==2){
            appInfo.setStatus(4L);
            appInfo.setOffsaledate(new Date());
            int i = appInfoservice.update(appInfo);
            return resultvo.sussue("上架成功",i);
        }
        else if (type==4){
           appInfo.setStatus(5L);
           int i = appInfoservice.update(appInfo);
           return resultvo.sussue("下架成功",i);
       }
       else if (type==5){
           appInfo.setStatus(1L);
           int i = appInfoservice.update(appInfo);
           return resultvo.sussue("上架请求成功,待通过",i);
       }
       else if (type==6){
           appInfo.setStatus(2L);
           int i = appInfoservice.update(appInfo);
           return resultvo.sussue("审核通过",i);
       }
       else if (type==7){
           appInfo.setStatus(3L);
           int i = appInfoservice.update(appInfo);
           return resultvo.sussue("审核不通过",i);
       }

        return null;
    }


}
