package com.demo1.service;

import com.demo1.dto.Logindto;
import com.demo1.vo.Loginvo;

public interface BackendUserservice {
    Loginvo queryAllByLimit(Logindto logindto);
}
