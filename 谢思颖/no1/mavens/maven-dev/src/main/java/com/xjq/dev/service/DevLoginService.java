package com.xjq.dev.service;

import com.xjq.common.dao.DevUserDao;
import com.xjq.common.dto.LoginDto;
import com.xjq.common.entity.DevUser;
import com.xjq.common.utiles.SessionUtils;
import com.xjq.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevLoginService {
    @Autowired
    DevUserDao devUserDao;

    public LoginUserVo devLogin(LoginDto loginDto) {
        DevUser devUser=new DevUser();
        devUser.setDevcode(loginDto.getUserCode());
        devUser.setDevpassword(loginDto.getPassword());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if (devUsers==null||devUsers.size()==0){
            throw new RuntimeException("账号或密码错误");
        }

        LoginUserVo vo=new LoginUserVo();

        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
        stringBuffer.append("-"+devUsers.get(0).getId());
        stringBuffer.append("-"+"dev");
        stringBuffer.append("-"+System.currentTimeMillis());

        vo.setUserCode(devUsers.get(0).getDevcode());
        vo.setUserName(devUsers.get(0).getDevname());
        vo.setUserType("dev");
        vo.setToken(stringBuffer.toString());
        SessionUtils.put(stringBuffer.toString(),devUsers.get(0));
        return vo;
    }
}
