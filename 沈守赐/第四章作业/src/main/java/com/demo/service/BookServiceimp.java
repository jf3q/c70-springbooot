package com.demo.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.dao.BookDao;
import com.demo.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceimp extends ServiceImpl<BookDao, Book> implements BookService {

    @Autowired
    BookDao bookDao;
    @Override
    public int deleteBatchIds(List<Integer> id) {
        return bookDao.deleteBatchIds(id);
    }
}
