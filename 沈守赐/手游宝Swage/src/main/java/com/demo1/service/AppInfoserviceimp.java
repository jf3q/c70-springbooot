package com.demo1.service;

import com.demo1.dao.AppInfoDao;
import com.demo1.entity.AppInfo;
import com.demo1.entity.page.Appinfopage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppInfoserviceimp implements AppInfoservice {
    @Autowired
    private AppInfoDao appInfoDao;

    @Override
    public Appinfopage page(AppInfo appInfo, int pagenum, int pagesize) {
        Appinfopage appinfopage = new Appinfopage();
        appinfopage.setPagenum(pagenum);
        appinfopage.setPagesize(pagesize);
        appinfopage.setList(appInfoDao.queryAllByLimit(appInfo,(pagenum-1)*pagesize,pagesize));
        appinfopage.setPagecount(appInfoDao.count(appInfo));

        return appinfopage;
    }

    @Override
    public int insert(AppInfo appInfo) {
        return appInfoDao.insert(appInfo);
    }

    @Override
    public int update(AppInfo appInfo) {
        return appInfoDao.update(appInfo);
    }

    @Override
    public int apk(String apkName) {
        return appInfoDao.apk(apkName);
    }

    @Override
    public int deleteById(int id) {
        return appInfoDao.deleteById(id);
    }

    @Override
    public AppInfo queryById(int id) {
        return appInfoDao.queryById(id);
    }
}
