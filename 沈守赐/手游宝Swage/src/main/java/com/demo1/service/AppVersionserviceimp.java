package com.demo1.service;


import com.demo1.dao.AppVersionDao;
import com.demo1.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionserviceimp implements AppVersionservice {

    @Autowired
    private AppVersionDao appVersionDao;

    @Override
    public List<AppVersion> queryById(int id) {
        return appVersionDao.queryById(id);
    }

    @Override
    public int insert(AppVersion appVersion) {
        return appVersionDao.insert(appVersion);
    }

    @Override
    public int deleteById(int id) {
        return appVersionDao.deleteById(id);
    }
}
