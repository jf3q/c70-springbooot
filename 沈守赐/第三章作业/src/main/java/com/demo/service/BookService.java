package com.demo.service;


import com.demo.dao.BookDao;
import com.demo.entity.Book;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    public PageInfo<Book> frst(String name, int pagenum, int pagesize){
        Book book = new Book();
        book.setName(name);
        PageHelper.startPage(pagenum,pagesize);
        List<Book> books = bookDao.queryAllByLimit(book);
        PageInfo<Book> pageInfo =new PageInfo<>(books);
        return pageInfo;
    }

    public int del(int id){
        int i = bookDao.deleteById(id);
        return i;
    }

    public int delby(int [] id){
        int i = bookDao.delete(id);
        return i;
    }

    public int insert(Book book){
        return bookDao.insert(book);
    }

    public int update(Book book){
        return bookDao.update(book);
    }

    public Book dan(int id){
        return bookDao.queryById(id);
    }

}
