package com.demo.Controller;



import com.demo.entity.Book;
import com.demo.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    BookService bookService;

    @RequestMapping("hello")
    public String hello(Model model){
        PageInfo<Book> frst = bookService.frst("", 1, 3);
        model.addAttribute("page",frst);
        return "index";
    }


    @RequestMapping("cx")
    public String cx(Model model,String name){
        PageInfo<Book> frst = bookService.frst(name, 1, 3);
        model.addAttribute("page",frst);
        model.addAttribute("bookname",name);
        return "index";
    }

    @RequestMapping("page")
    public String cx(Model model,String name,int pagenum){
        PageInfo<Book> frst = bookService.frst(name, pagenum, 3);
        model.addAttribute("page",frst);
        model.addAttribute("bookname",name);
        return "index";
    }

    @RequestMapping("del")
    public String del(int id){
        int i = bookService.del(id);
        return "redirect:/hello";
    }

    @RequestMapping("delby")
    public String del(int[] id){
        int i = bookService.delby(id);
        return "redirect:/hello";
    }


    @RequestMapping("add")
    public String del(Book book){
            bookService.insert(book);
        return "redirect:/hello";
    }


    @RequestMapping("add1")
    public String add1(){
        return "insert";
    }

    @RequestMapping("update")
    public String update(int id,Model model){
        Book book = bookService.dan(id);
        model.addAttribute("dan",book);
        return "update";
    }

    @RequestMapping("update1")
    public String update1(Book book1){
        int i = bookService.update(book1);
        return "redirect:/hello";
    }


}
