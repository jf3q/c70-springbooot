package com.dao;

import com.entity.AppCategory;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 手游类别(AppCategory)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-12 09:08:55
 */
public interface AppCategoryDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
   List<AppCategory> queryById(int id);

    /**
     * 查询指定行数据
     *
     * @param appCategory 查询条件

     * @return 对象列表
     */
    List<AppCategory> queryAllByLimit(AppCategory appCategory);

    /**
     * 统计总行数
     *
     * @param appCategory 查询条件
     * @return 总行数
     */
    long count(AppCategory appCategory);

    /**
     * 新增数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insert(AppCategory appCategory);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<AppCategory> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<AppCategory> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<AppCategory> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<AppCategory> entities);

    /**
     * 修改数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int update(AppCategory appCategory);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

