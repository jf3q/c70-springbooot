package com.entity.page;

import com.entity.AppInfo;

import java.util.List;

public class Appinfopage {
    private int pagenum;
    private int pagesize;
    private int pagecount;
    private int pagesum;
    private List<AppInfo> list;

    public int getPagenum() {
        return pagenum;
    }

    public void setPagenum(int pagenum) {
        this.pagenum = pagenum;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getPagecount() {
        return pagecount;
    }

    public void setPagecount(int pagecount) {
        this.pagecount = pagecount;
        if (pagecount!=0){
            if (pagecount%this.pagesize!=0){
                this.pagesum = (pagecount/this.pagesize)+1;
            }
            else if (pagecount%this.pagesize==0) {
                this.pagesum = (pagecount/this.pagesize);
            }
        }
    }

    public int getPagesum() {
        return pagesum;
    }

    public void setPagesum(int pagesum) {
        this.pagesum = pagesum;
    }


    public List<AppInfo> getList() {
        return list;
    }

    public void setList(List<AppInfo> list) {
        this.list = list;
    }
}
