package com.admin.Controller;


import com.admin.service.Adminserviceimp;
import com.dto.Logindto;
import com.vo.Loginvo;
import com.vo.Resultvo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Devcontroller {
    ApplicationContext context = new ClassPathXmlApplicationContext("Application.xml");
    Adminserviceimp devUserService = context.getBean(Adminserviceimp.class);

    @RequestMapping("/login")
    @ResponseBody
    public Resultvo cx(@RequestBody Logindto logindto){
        Resultvo resultvo = new Resultvo();
            try{
                Loginvo loginvo = devUserService.login(logindto);
                return resultvo.sussue("登录成功",loginvo);
            }catch (Exception e){
                return resultvo.error(e.getMessage());
            }
        }
}
