package com.admin.service;

import com.dao.BackendUserDao;
import com.dao.DevUserDao;
import com.dto.Logindto;
import com.entity.BackendUser;
import com.entity.DevUser;
import com.ui.Session;
import com.vo.Loginvo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Adminserviceimp {
    @Autowired
    private BackendUserDao backendUserDao;

    public Loginvo login(Logindto logindto) {
        Loginvo loginvo = new Loginvo();
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(logindto.getUsercode());
        backendUser.setUserpassword(logindto.getPassword());
        System.out.println(backendUser);
        BackendUser backendUser1= backendUserDao.queryAllByLimit(backendUser);
        if (backendUser1==null){
            throw new RuntimeException("账号密码错误");
        }
        else{
            loginvo.setUsername(backendUser1.getUsername());
            loginvo.setUsertype(logindto.getUsertype());
            String realpath = UUID.randomUUID().toString().replace("-","");
            String token = realpath+"-"+logindto.getUsertype()+"-"+System.currentTimeMillis();
            loginvo.setToken(token);
            Session.SETmap(token,backendUser1);
        }

        return loginvo;
    }


}
