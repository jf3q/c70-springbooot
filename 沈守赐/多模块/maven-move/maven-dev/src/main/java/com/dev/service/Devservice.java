package com.dev.service;

import com.dto.Logindto;
import com.vo.Loginvo;

public interface Devservice {
    Loginvo login(Logindto logindto);
}
