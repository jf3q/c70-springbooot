package com.dev.service;

import com.dao.DevUserDao;
import com.dto.Logindto;
import com.entity.DevUser;
import com.ui.Session;
import com.vo.Loginvo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class Devserviceimp {
    @Autowired
    private DevUserDao devUserDao;

    public Loginvo login(Logindto logindto) {
        Loginvo loginvo = new Loginvo();
        DevUser devUser = new DevUser();
        devUser.setDevcode(logindto.getUsercode());
        devUser.setDevpassword(logindto.getPassword());
        DevUser devUser1= devUserDao.queryAllByLimit(devUser);
        if (devUser1==null){
            throw new RuntimeException("账号密码错误");
        }
        else{
            loginvo.setUsername(devUser1.getDevname());
            loginvo.setUsertype(logindto.getUsertype());
            String realpath = UUID.randomUUID().toString().replace("-","");
            String token = realpath+"-"+logindto.getUsertype()+"-"+System.currentTimeMillis();
            loginvo.setToken(token);
            Session.SETmap(token,devUser1);
        }

        return loginvo;
    }


}
