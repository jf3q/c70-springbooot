package com.demo1.service;

import com.demo1.dao.BackendUserDao;
import com.demo1.dto.Logindto;
import com.demo1.entity.BackendUser;
import com.demo1.ui.Session;
import com.demo1.vo.Loginvo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BackendUserserviceimp implements BackendUserservice {

    @Autowired
    private BackendUserDao backendUserDao;

    @Override
    public Loginvo queryAllByLimit(Logindto logindto) {
        Loginvo loginvo = new Loginvo();
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(logindto.getUsercode());
        backendUser.setUserpassword(logindto.getPassword());
        BackendUser backendUser1= backendUserDao.queryAllByLimit(backendUser);
        if (backendUser1==null){
            throw new RuntimeException("账号密码错误");
        }
        else{
            loginvo.setUsername(backendUser1.getUsername());
            loginvo.setUsertype(logindto.getUsertype());
            String realpath = UUID.randomUUID().toString().replace("-","");
            String token = realpath+"-"+logindto.getUsertype()+"-"+System.currentTimeMillis();
            loginvo.setToken(token);
            Session.SETmap(token,backendUser1);
        }

        return loginvo;
    }
}
