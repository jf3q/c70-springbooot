package com.demo1.service;


import com.demo1.entity.AppVersion;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AppVersionservice {
    List<AppVersion> queryById(@Param("id") int id);
    int insert(AppVersion appVersion);
    int deleteById(int id);
}
