package com.demo1.Controller;

import com.demo1.dto.Logindto;
import com.demo1.service.BackendUserservice;
import com.demo1.service.DevUserservice;
import com.demo1.vo.Loginvo;
import com.demo1.vo.Resultvo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class LoginController {

    @Autowired
   private DevUserservice devUserService ;
    @Autowired
    private BackendUserservice  backendUserservice ;


    @RequestMapping("/login")
    @ResponseBody
    public Resultvo cx(@RequestBody Logindto logindto){
        Resultvo resultvo = new Resultvo();
        if (logindto.getUsertype().equals("dev")){
            try{
                Loginvo loginvo = devUserService.login(logindto);
                return resultvo.sussue("登录成功",loginvo);
            }catch (Exception e){
                return resultvo.error(e.getMessage());
            }
        }
        else if (logindto.getUsertype().equals("admin")){
            try{
                Loginvo loginvo = backendUserservice.queryAllByLimit(logindto);
                return resultvo.sussue("登录成功",loginvo);
            }catch (Exception e){
                return resultvo.error(e.getMessage());
            }
        }
        else {
            return resultvo.error("没有此角色");
        }

    }


//    @RequestMapping("/loginout")
//    @ResponseBody
//    public Resultvo loginout(HttpServletRequest request){
//        Resultvo resultvo = new Resultvo();
//        String token = request.getHeader("token");
//        Session.Remove(token);
//        return resultvo.sussue("注销成功",1);
//    }


}
