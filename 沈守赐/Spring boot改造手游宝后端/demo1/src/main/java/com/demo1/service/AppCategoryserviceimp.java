package com.demo1.service;


import com.demo1.dao.AppCategoryDao;
import com.demo1.entity.AppCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppCategoryserviceimp implements AppCategoryservice {

    @Autowired
    private AppCategoryDao appCategoryDao;


    @Override
    public List<AppCategory> queryById(int id) {
        return appCategoryDao.queryById(id);
    }

    @Override
    public List<AppCategory> queryAllByLimit(AppCategory appCategory) {
        return appCategoryDao.queryAllByLimit(appCategory);
    }


}
