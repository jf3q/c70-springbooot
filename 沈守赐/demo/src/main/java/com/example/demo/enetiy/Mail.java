package com.example.demo.enetiy;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;

@Data
@AllArgsConstructor
public class Mail {
    private String from;//发件人
    private String to;//收件人
    private String cc;//抄送人
    private String subject;//标题
    private String content;//邮件内容
    private File file;//附件
}