package com.example.demo.controller;

import com.example.demo.enetiy.Mail;
import com.example.demo.service.MailService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Controller
public class MailController {

    @Autowired
    MailService mailService;

    @ResponseBody
    @RequestMapping("/sendSimpleMail")
    public String sendSimpleMail(){
        Mail mail = new Mail(
                "a3501898835@163.com",
                "3501898835@qq.com",
                "a3501898835@163.com",
                "测试发送邮件",
                "我的邮件内容",
                null
        );
        mailService.sendSimpleMailMessage(mail);
        return "简单邮件发送成功！";
    }


    @ResponseBody
    @RequestMapping("/sendMailWithFile")
    public String sendMailWithFile(HttpServletRequest request) throws IOException {
        File path= new File("D:\\Springboot图片库\\aaa.png");
        Mail mail = new Mail(
                "18216363590@163.com",
                "3501898835@qq.com",
                "18216363590@163.com",
                "测试发送邮件",
                "我的邮件内容",
                path
        );
        //存储上传文件的目标文件夹(服务器上的，每个人的路径都不同，按个人实际情况设置)

        mailService.sendMailWithFile(mail);
        return "带附件邮件发送成功！";
    }


    @ResponseBody
    @RequestMapping("/sendTemplateMail")
    public String sendTemplateMail(Mail mail,String username,String orderNo,String amount,String mobile){
        mailService.sendTempateMail(mail,username,orderNo,amount,mobile);
        return "模板邮件发送成功！";
    }
}