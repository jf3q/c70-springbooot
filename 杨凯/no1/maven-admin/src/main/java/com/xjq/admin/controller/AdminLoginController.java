package com.xjq.admin.controller;

import com.xjq.admin.service.AdminLoginService;
import com.xjq.common.dto.LoginDto;
import com.xjq.common.vo.LoginUserVo;
import com.xjq.common.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminLoginController {
    @Autowired
    AdminLoginService adminLoginService;

    @PostMapping("/userLogin")
    public ResultVo userLogin(@RequestBody LoginDto loginDto){
            try {
                LoginUserVo vo= adminLoginService.adminLogin(loginDto);
                return ResultVo.success("登录成功",vo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error(e.getMessage());
            }

    }

}
