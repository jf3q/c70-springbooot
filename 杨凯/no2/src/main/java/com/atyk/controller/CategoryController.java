package com.atyk.controller;

import com.atyk.service.AppCategoryService;
import com.atyk.vo.CategoryTreeVo;
import com.atyk.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @GetMapping("/tree")
    public ResultVo tree(){
        CategoryTreeVo vo = appCategoryService.tree();
        return ResultVo.success(vo);
    }

}
