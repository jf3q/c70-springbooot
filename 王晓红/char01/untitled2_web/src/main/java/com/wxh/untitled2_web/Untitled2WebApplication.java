package com.wxh.untitled2_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Untitled2WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(Untitled2WebApplication.class, args);
    }

}
