package com.wxh.untitled2_web.controller;

import com.wxh.untitled2_web.dao.UserInfo;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @PostMapping
    public String addUser(@RequestBody UserInfo userInfo){
        System.out.println("新增用户信息："+userInfo);
        return "新增用户成功";
    }
    @DeleteMapping("/{id}")
    public String del(@PathVariable Integer id){
        System.out.println("删除用户ID："+id);
        return "删除用户成功";
    }
    @PutMapping
    public String update(@RequestBody UserInfo userInfo){
        System.out.println("修改后");
        return "修改用户成功";
    }
    @GetMapping("/{id}")
    public String getUser(@PathVariable Integer id){
        System.out.println("用户ID："+id);
        return "查询用户ID成功！";
    }
    @GetMapping
    public String getUsers(){
        System.out.println("查询所有的用户");
        return "查询所有的用户";
    }
}
