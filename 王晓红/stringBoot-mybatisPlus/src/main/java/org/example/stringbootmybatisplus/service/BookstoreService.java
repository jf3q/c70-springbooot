package org.example.stringbootmybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.example.stringbootmybatisplus.entity.Bookstore;

import java.util.List;


public interface BookstoreService extends IService<Bookstore> {
    List<Bookstore> searchBook(Bookstore bookstore);

    IPage getPage(Integer pageNum,Bookstore bookstore);

    void addAllBook(Bookstore bookstore);


    boolean updateBook(Bookstore bookstores);
}
