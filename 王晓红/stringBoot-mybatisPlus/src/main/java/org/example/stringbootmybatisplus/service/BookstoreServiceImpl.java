package org.example.stringbootmybatisplus.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.stringbootmybatisplus.dao.BookstoreMapper;
import org.example.stringbootmybatisplus.entity.Bookstore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookstoreServiceImpl extends ServiceImpl<BookstoreMapper, Bookstore> implements BookstoreService {
    @Autowired
    BookstoreMapper bookstoreMapper;

    @Override
    public List<Bookstore> searchBook(Bookstore bookstore) {
        //LambdaQueryWrapper用于条件查询

        LambdaQueryWrapper<Bookstore> queryWrapper = new LambdaQueryWrapper<>();
        if (bookstore.getName() != null && bookstore.getName() != "") {
            queryWrapper.like(Bookstore::getName, bookstore.getName());
        }
        if (bookstore.getCategory() != null && bookstore.getCategory() != "") {
            queryWrapper.eq(Bookstore::getCategory, bookstore.getCategory());
        }
        if (bookstore.getAuthor() != null && bookstore.getAuthor() != "") {
            queryWrapper.like(Bookstore::getAuthor, bookstore.getAuthor());
        }
        return bookstoreMapper.selectList(queryWrapper);
    }

    //    分页查询
    @Override
    public IPage getPage(Integer pageNum, Bookstore bookstore) {
        IPage page = new Page(pageNum, 3);
        LambdaQueryWrapper<Bookstore> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Bookstore::getId);
        if (bookstore.getName() != null && bookstore.getName() != "") {
            queryWrapper.like(Bookstore::getName, bookstore.getName());
        }
        if (bookstore.getCategory() != null && bookstore.getCategory() != "") {
            queryWrapper.eq(Bookstore::getCategory, bookstore.getCategory());
        }
        if (bookstore.getAuthor() != null && bookstore.getAuthor() != "") {
            queryWrapper.like(Bookstore::getAuthor, bookstore.getAuthor());
        }
        return bookstoreMapper.selectPage(page, queryWrapper);
    }

    //    新增
    @Override
    public void addAllBook(Bookstore bookstore) {
        bookstoreMapper.insert(bookstore);
    }

    @Override
    public boolean updateBook(Bookstore bookstores) {
        return false;
    }

}
