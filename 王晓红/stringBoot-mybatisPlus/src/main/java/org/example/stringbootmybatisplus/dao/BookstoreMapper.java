package org.example.stringbootmybatisplus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.stringbootmybatisplus.entity.Bookstore;

@Mapper
public interface BookstoreMapper extends BaseMapper<Bookstore> {
    Bookstore updateId(Integer id);

    void updateBook(Bookstore bookstore);
}
