package com.itcy7.controller;

import com.itcy7.entity.AppInfo;
import com.itcy7.service.AppInfoService;
import com.itcy7.utils.dto.AppInfoDto;
import com.itcy7.utils.dto.ReviewDto;
import com.itcy7.utils.vo.ResultVo;
import com.github.pagehelper.PageInfo;

import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppInfoController {

    @Autowired
    AppInfoService appInfoService;

    @RequestMapping("/selAppInfo/{pageNow}")
    public ResultVo selAppInfo(@RequestBody AppInfoDto appInfoDto, @PathVariable Integer pageNow, HttpServletRequest request){
        String token=request.getHeader("token");
        String[] split = token.split("-");
        if(split[3].equals("dev")){
            appInfoDto.setDevid(Long.parseLong(split[2]));
        }
        PageInfo<AppInfo> page=appInfoService.findPage(appInfoDto,pageNow);
        return ResultVo.success("",page);
    }

    @RequestMapping("/addOrUpdate")
    public ResultVo addOrUpdate(AppInfo appInfo, MultipartFile logo,HttpServletRequest request){
        String path=request.getServletContext().getRealPath("/upload/appinfo/logo");
        String token=request.getHeader("token");
        String[] split = token.split("-");

        if(logo!=null&&!logo.isEmpty()){
            File file = new File(path);
            if (!file.exists()){
                file.mkdirs();
            }
            String picName=logo.getOriginalFilename();
            String behindName= FilenameUtils.getExtension(picName);
            if(logo.getSize()>500*1024){
                return  ResultVo.error("logo大于500KB");
            }else if(behindName.equalsIgnoreCase("jpg")||behindName.equalsIgnoreCase("png")||
                    behindName.equalsIgnoreCase("jpeg")||behindName.equalsIgnoreCase("gif")){
                String newName= UUID.randomUUID().toString().replace("-","")+"."+behindName;
                try {
                    String pic=path+File.separator+newName;
                    System.out.println(pic);
                    logo.transferTo(new File(path+File.separator+newName));
                    appInfo.setLogopicpath("/upload/appinfo/logo/"+newName);
                } catch (IOException e) {
                    e.printStackTrace();
                    ResultVo.error("上传异常");
                }
            }else{
                ResultVo.error("logo格式不正确");
            }
        }

        if(appInfo.getId()==null){
            appInfo.setCreationdate(new Date());
            appInfo.setCreatedby(Long.valueOf(split[2]));
            appInfo.setStatus(1L);
            appInfo.setDevid(Long.valueOf(split[2]));
            appInfoService.addAppInfo(appInfo);
        }else{
            appInfo.setModifydate(new Date());
            appInfo.setModifyby(Long.valueOf(split[2]));
            appInfo.setStatus(1L);
            appInfo.setDevid(Long.valueOf(split[2]));
            appInfoService.updateAppInfo(appInfo);
        }
        return ResultVo.success("",null);
    }

    @RequestMapping("/checkApk")
    public ResultVo checkApk(@RequestBody AppInfo appInfo){
        Boolean flag=appInfoService.findApk(appInfo);
        return ResultVo.success("",flag);
    }

    @RequestMapping("/selAppDetalis/{appid}")
    public ResultVo selAppDetalis(@PathVariable Long appid){
        AppInfo appInfo=appInfoService.findAppOne(appid);
        return ResultVo.success("",appInfo);
    }

    @DeleteMapping("/delApp/{appid}")
    public ResultVo delApp(@PathVariable Long appid,HttpServletRequest request){
        try {
            appInfoService.delAppInfo(appid,request);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error("删除异常");
        }
        return ResultVo.success("",null);
    }

    @PutMapping("/upOrOff/{appid}")
    public ResultVo upOrOff(@PathVariable Long appid){
        AppInfo appOne = appInfoService.findAppOne(appid);
        if(appOne.getStatus()==4L){
            appOne.setStatus(5L);
        }else if(appOne.getStatus()==2L|| appOne.getStatus()==5L){
            appOne.setStatus(4L);
        }
        appInfoService.updateAppInfo(appOne);
        return ResultVo.success("",null);
    }

    @PutMapping("/review")
    public ResultVo review(@RequestBody ReviewDto reviewDto){
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(reviewDto,appInfo);
        appInfoService.updateAppInfo(appInfo);
        return ResultVo.success("",null);
    }
}
