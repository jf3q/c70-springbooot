package com.itcy7.controller;

import com.itcy7.service.AppCategoryService;
import com.itcy7.utils.vo.ResultVo;
import com.itcy7.utils.vo.TreeVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @RequestMapping("/getTree")
    public ResultVo getTree(){
        TreeVo treeVo=appCategoryService.findTree();
        return ResultVo.success("",treeVo);
    }
}
