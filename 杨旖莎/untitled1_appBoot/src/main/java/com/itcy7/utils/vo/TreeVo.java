package com.itcy7.utils.vo;

import java.util.List;

public class TreeVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    List<TreeVo> childList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<TreeVo> getChildList() {
        return childList;
    }

    public void setChildList(List<TreeVo> childList) {
        this.childList = childList;
    }
}
