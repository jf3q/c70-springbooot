package com.itcy7.utils.session;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static Map map=new HashMap();

    public static void setToken(String key,Object user){
        map.put(key,user);
    }

    public static void removeToken(String key){
        map.remove(key);
    }

    public static Object getToken(String key){
        return map.get(key);
    }
}
