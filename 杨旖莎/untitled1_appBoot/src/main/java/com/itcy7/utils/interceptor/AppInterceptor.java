package com.itcy7.utils.interceptor;

import com.itcy7.utils.session.SessionUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class AppInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token=request.getHeader("token");

        if(token==null){
            response.setStatus(401);
        }else if(SessionUtils.getToken(token)==null){
            response.setStatus(403);
        }else{
            String[] split = token.split("-");
            if(System.currentTimeMillis()-Long.parseLong(split[4])>3600*2*1000){
                response.setStatus(403);
            }
            return true;
        }
        return false;
    }
}
