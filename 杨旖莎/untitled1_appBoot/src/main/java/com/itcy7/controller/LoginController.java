package com.itcy7.controller;

import com.itcy7.service.LoginService;
import com.itcy7.utils.dto.LoginDto;
import com.itcy7.utils.session.SessionUtils;
import com.itcy7.utils.vo.LoginVo;
import com.itcy7.utils.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        try {
            LoginVo loginVo=loginService.loginOn(loginDto);
            return ResultVo.success("",loginVo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }

    @RequestMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token=request.getHeader("token");
        SessionUtils.removeToken(token);
        return ResultVo.success("",null);
    }
}
