package com.service;

import com.dao.AppVersionDao;
import com.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> selVersionList(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        List<AppVersion> appVersionList = appVersionDao.queryAll(appVersion);
        return appVersionList;
    }

    public void addVersion(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }

    public void updateVersion(AppVersion appVersion) {
        appVersionDao.update(appVersion);
    }

    public List<AppVersion> findNewVersion(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        List<AppVersion> appVersionList = appVersionDao.queryDesc(appVersion);
        return appVersionList;
    }
}
