package com.service;

import com.dao.AppCategoryDao;
import com.entity.AppCategory;
import com.utils.vo.TreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppCategoryService {
    @Autowired
    AppCategoryDao appCategoryDao;

    public TreeVo findTree() {
        TreeVo treeVo=new TreeVo();

        List<TreeVo> treeVoList=new ArrayList<>();
        List<AppCategory> appCategoryList = appCategoryDao.queryAll(new AppCategory());
        for (AppCategory appCategory : appCategoryList) {
            TreeVo tree=new TreeVo();
            BeanUtils.copyProperties(appCategory,tree);
            treeVoList.add(tree);
        }

        for (TreeVo vo : treeVoList) {
            if(vo.getParentid()==null){
                treeVo=findChild(vo,treeVoList);
            }
        }
        return treeVo;
    }

    private TreeVo findChild(TreeVo vo, List<TreeVo> treeVoList) {
        vo.setChildList(new ArrayList<>());
        for (TreeVo treeVo : treeVoList) {
            if(treeVo.getParentid()==vo.getId()){
                vo.getChildList().add(treeVo);
                findChild(treeVo,treeVoList);
            }
        }
        return vo;
    }
}
