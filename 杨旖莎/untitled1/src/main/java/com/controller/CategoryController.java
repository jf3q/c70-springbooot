package com.controller;

import com.service.AppCategoryService;
import com.utils.vo.ResultVo;
import com.utils.vo.TreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @RequestMapping("/getTree")
    public ResultVo getTree(){
        TreeVo treeVo=appCategoryService.findTree();
        return ResultVo.success("",treeVo);
    }
}
