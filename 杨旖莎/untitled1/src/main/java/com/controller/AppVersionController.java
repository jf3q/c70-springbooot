package com.controller;

import com.entity.AppInfo;
import com.entity.AppVersion;
import com.service.AppInfoService;
import com.service.AppVersionService;
import com.utils.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AppVersionController {

    @Autowired
    AppVersionService appVersionService;

    @Autowired
    AppInfoService appInfoService;

    @RequestMapping("/selVersion/{appid}")
    public ResultVo selVersion(@PathVariable Long appid){
        List<AppVersion> appVersionList= appVersionService.selVersionList(appid);
        return ResultVo.success("",appVersionList);
    }

    @RequestMapping("/selNewVersion/{appid}")
    public ResultVo selNewVersion(@PathVariable Long appid){
        List<AppVersion> appVersionList=appVersionService.findNewVersion(appid);
        return ResultVo.success("",appVersionList);
    }

    @PostMapping("/addVersion")
    public ResultVo addVersion(AppVersion appVersion, MultipartFile apk, HttpServletRequest request){
        String path=request.getServletContext().getRealPath("/upload/appinfo/apk");
        String token=request.getHeader("token");
        String[] split = token.split("-");

        if(apk!=null&&!apk.isEmpty()){
            File file = new File(path);
            if (!file.exists()){
                file.mkdirs();
            }
            String picName=apk.getOriginalFilename();
            String behindName= FilenameUtils.getExtension(picName);
            if(apk.getSize()>500*1024*1024){
                return  ResultVo.error("版本大于500MB");
            }else if(behindName.equals("apk")){
                String newName= UUID.randomUUID().toString().replace("-","")+"."+behindName;
                try {
                    apk.transferTo(new File(path+File.separator+newName));
                    appVersion.setDownloadlink("/upload/appinfo/apk/"+newName);
                } catch (IOException e) {
                    e.printStackTrace();
                    ResultVo.error("上传异常");
                }
            }else{
                ResultVo.error("文件格式不正确");
            }
        }

        AppInfo appInfo = new AppInfo();

        if(appVersion.getId()==null){
            appVersion.setCreationdate(new Date());
            appVersion.setCreatedby(Long.valueOf(split[2]));
            appVersion.setPublishstatus(3L);
            appVersionService.addVersion(appVersion);

            appInfo.setId(appVersion.getAppid());
            appInfo.setDownloads(0L);
            appInfo.setVersionid(appVersion.getId());
            appInfo.setStatus(1L);
            appInfo.setDevid(Long.valueOf(split[2]));

        }else{
            appVersion.setModifydate(new Date());
            appVersion.setModifyby(Long.valueOf(split[2]));
            appVersion.setPublishstatus(3L);
            appVersionService.updateVersion(appVersion);

            appInfo.setId(appVersion.getAppid());
            appInfo.setStatus(1L);
            appInfo.setDownloads(0L);
            appInfo.setVersionid(appVersion.getId());
            appInfo.setDevid(Long.valueOf(split[2]));
        }
        appInfoService.updateAppInfo(appInfo);
        return ResultVo.success("",null);
    }
}
