package com.controller;

import com.service.LoginService;
import com.utils.dto.LoginDto;
import com.utils.session.SessionUtils;
import com.utils.vo.LoginVo;
import com.utils.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        try {
            LoginVo loginVo=loginService.loginOn(loginDto);
            return ResultVo.success("",loginVo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }

    @RequestMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token=request.getHeader("token");
        SessionUtils.removeToken(token);
        return ResultVo.success("",null);
    }
}
