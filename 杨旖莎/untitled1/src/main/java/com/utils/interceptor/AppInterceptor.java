package com.utils.interceptor;

import com.utils.session.SessionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AppInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token=request.getHeader("token");

        if(token==null){
            response.setStatus(401);
        }else if(SessionUtils.getToken(token)==null){
            response.setStatus(403);
        }else{
            String[] split = token.split("-");
            if(System.currentTimeMillis()-Long.parseLong(split[4])>3600*2*1000){
                response.setStatus(403);
            }
            return true;
        }
        return false;
    }
}
