package com.utils.vo;

public class LoginVo {
    private String token;
    private Object user;
    private Integer type;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public LoginVo() {
    }

    public LoginVo(String token, Object user, Integer type) {
        this.token = token;
        this.user = user;
        this.type = type;
    }
}
