package com.itheima.com.controller;


import com.itheima.com.controller.result.ResUlt;
import com.itheima.com.service.AppCategoryService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/three")
public class ThreeController {
    @Resource
    AppCategoryService appCategoryService;
    @RequestMapping("/getThree")
    public ResUlt getThree(){
        return new ResUlt(true,appCategoryService.getThree(),null);
    }
}
