package com.itheima.com.vo;

public class LoginUserVo {
    private String account;
    private String userType;
    private String token;
    private Long createDate;

    public LoginUserVo() {
    }

    public LoginUserVo(String account, String userType, String token, Long createDate) {
        this.account = account;
        this.userType = userType;
        this.token = token;
        this.createDate = createDate;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "LoginUserVo{" +
                "account='" + account + '\'' +
                ", userType='" + userType + '\'' +
                ", token='" + token + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
