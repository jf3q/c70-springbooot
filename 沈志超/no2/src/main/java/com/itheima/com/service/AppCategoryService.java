package com.itheima.com.service;


import com.itheima.com.dao.AppCategoryDao;
import com.itheima.com.dao.AppVersionDao;
import com.itheima.com.entity.AppCategory;
import com.itheima.com.vo.CategoryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppCategoryService {
    @Autowired
    AppCategoryDao appCategoryDao;
    @Autowired
    AppVersionDao appVersionDao;

    public Object getThree() {
        CategoryVo categoryVo = new CategoryVo();
        List<CategoryVo> list = new ArrayList<>();

        List<AppCategory> appCategories = appCategoryDao.queryAll(new AppCategory());
        appCategories.forEach(appCategory -> {
            CategoryVo categoryVo1 = new CategoryVo();
            BeanUtils.copyProperties(appCategory,categoryVo1);
            list.add(categoryVo1);
        });
        for (CategoryVo categoryVo1:list){
            if (categoryVo1.getParentid()==null){
                categoryVo = getThrees(categoryVo1,list);
            }
        }
        return categoryVo;
    }

    private CategoryVo getThrees(CategoryVo categoryVo, List<CategoryVo> list) {
        categoryVo.setList(new ArrayList<>());
        for (CategoryVo categoryVo1 : list) {
            if (categoryVo.getId() == categoryVo1.getParentid()) {
                categoryVo.getList().add(categoryVo1);
                getThrees(categoryVo1, list);
            }
        }
        return categoryVo;
    }
}