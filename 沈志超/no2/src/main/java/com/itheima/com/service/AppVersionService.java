package com.itheima.com.service;


import com.itheima.com.dao.AppInfoDao;
import com.itheima.com.dao.AppVersionDao;
import com.itheima.com.entity.AppInfo;
import com.itheima.com.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;
    @Autowired
    AppInfoDao appInfoDao;

    public List<AppVersion> getAppInfo(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        return appVersionDao.queryAll(appVersion);
    }

    public boolean addApk(AppVersion appVersion, Long id) {
       appVersion.setPublishstatus(1L);
       appVersion.setCreatedby(id);
       appVersion.setCreationdate(new Date());
       appVersionDao.insert(appVersion);

        AppInfo appInfo = new AppInfo();
        appInfo.setStatus(1L);
        appInfo.setId(appVersion.getAppid());
        appInfo.setModifydate(new Date());
        appInfo.setModifyby(id);
        appInfo.setVersionid(appVersion.getId());
        return appInfoDao.update(appInfo)>0;
    }
}
