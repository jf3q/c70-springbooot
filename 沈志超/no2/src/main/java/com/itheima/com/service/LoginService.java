package com.itheima.com.service;


import com.itheima.com.dao.BackendUserDao;
import com.itheima.com.dao.DevUserDao;
import com.itheima.com.dto.LoginDto;
import com.itheima.com.entity.BackendUser;
import com.itheima.com.entity.DevUser;
import com.itheima.com.uitls.SessionUtils;
import com.itheima.com.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginService {
    @Autowired
    DevUserDao devUserDao;
    @Autowired
    BackendUserDao backendUserDao;
    public LoginUserVo login(LoginDto loginDto){
        LoginUserVo loginUserVo = new LoginUserVo();
        if (loginDto.getUserType()==1){
            DevUser devUser = new DevUser();
            devUser.setDevcode(loginDto.getAccount());
            devUser.setDevpassword(loginDto.getPassword());
            List<DevUser> devUsers = devUserDao.queryAll(devUser);
            if (devUsers.size()>0){
                if (devUsers.get(0)!=null){
                    String s = UUID.randomUUID().toString().replace("-", "") + "-" + System.currentTimeMillis();
                    loginUserVo.setToken(s);
                    loginUserVo.setAccount(devUsers.get(0).getDevname());
                    loginUserVo.setUserType("user");
                    loginUserVo.setCreateDate(System.currentTimeMillis());
                    SessionUtils.put(loginUserVo.getToken(),devUsers.get(0));
                }
            }
        }else if (loginDto.getUserType()==2){
            BackendUser backendUser = new BackendUser();
            backendUser.setUsercode(loginDto.getAccount());
            backendUser.setUserpassword(loginDto.getPassword());
            List<BackendUser> backendUsers = backendUserDao.queryAll(backendUser);
            if (backendUsers.size()>0){
                if (backendUsers.get(0)!=null){
                    String s = UUID.randomUUID().toString().replace("-", "") + "-" + System.currentTimeMillis();
                    loginUserVo.setToken(s);
                    loginUserVo.setAccount(backendUsers.get(0).getUsercode());
                    loginUserVo.setUserType("admin");
                    loginUserVo.setCreateDate(System.currentTimeMillis());
                    SessionUtils.put(loginUserVo.getToken(),backendUsers.get(0));
                }
            }
        }
        return loginUserVo;
    }

    public LoginUserVo login2(LoginDto loginDtoL) {
        LoginUserVo loginUserVo = new LoginUserVo();
        if (loginDtoL.getUserType()==1){
            DevUser devUser = new DevUser();
            devUser.setDevcode(loginDtoL.getAccount());
            devUser.setDevpassword(loginDtoL.getPassword());
            List<DevUser> devUsers = devUserDao.queryAll(devUser);
            if(devUsers.size()>0){
                if (devUsers.get(0)!=null){
                    String s = UUID.randomUUID().toString().replace("-", "") + "-" + System.currentTimeMillis();
                    loginUserVo.setToken(s);
                    loginUserVo.setAccount(devUsers.get(0).getDevname());
                    loginUserVo.setUserType("user");
                    loginUserVo.setCreateDate(System.currentTimeMillis());
                    SessionUtils.put(loginUserVo.getToken(),devUsers.get(0));
                }
            }
        }else if (loginDtoL.getUserType()==2){
            BackendUser backendUser = new BackendUser();
            backendUser.setUsercode(loginDtoL.getAccount());
            backendUser.setUserpassword(loginDtoL.getPassword());
            List<BackendUser> backendUsers = backendUserDao.queryAll(backendUser);
            if (backendUsers.size()>0){
                if (backendUsers.get(0)!=null){
                    String s = UUID.randomUUID().toString().replace("-", "") + "-" + System.currentTimeMillis();
                    loginUserVo.setToken(s);
                    loginUserVo.setAccount(backendUsers.get(0).getUsername());
                    loginUserVo.setUserType("admin");
                    loginUserVo.setCreateDate(System.currentTimeMillis());
                }
            }
        }
        return null;
    }
}
