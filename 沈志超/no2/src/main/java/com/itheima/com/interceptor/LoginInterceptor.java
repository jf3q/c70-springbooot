package com.itheima.com.interceptor;

import com.itheima.com.uitls.SessionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token==null){
            response.setStatus(401);
            return false;
        }else if (SessionUtils.get(token)==null){
            response.setStatus(403);
            return false;
        }else {
            return true;
        }
    }
}
