package com.itheima.com.vo;

import java.util.List;

public class CategoryVo {
    private Long id;
    private Long parentid;
    private String categoryname;
    private List<CategoryVo> list;

    public CategoryVo() {
    }

    public CategoryVo(Long id, Long parentid, String categoryname, List<CategoryVo> list) {
        this.id = id;
        this.parentid = parentid;
        this.categoryname = categoryname;
        this.list = list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentId) {
        this.parentid = parentId;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public List<CategoryVo> getList() {
        return list;
    }

    public void setList(List<CategoryVo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "CategoryVo{" +
                "id=" + id +
                ", parentId=" + parentid +
                ", categoryname='" + categoryname + '\'' +
                ", list=" + list +
                '}';
    }
}
