package com.itheima.com.controller;

import com.itheima.com.controller.result.ResUlt;
import com.itheima.com.entity.AppInfo;
import com.itheima.com.entity.AppVersion;
import com.itheima.com.entity.DevUser;
import com.itheima.com.service.AppInfoService;
import com.itheima.com.service.AppVersionService;
import com.itheima.com.uitls.SessionUtils;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/appInfo")
public class AppInfoController {
    @Resource
    AppInfoService appInfoService;
    @Resource
    AppVersionService appVersionService;
    @RequestMapping("/getPage/{pageNum}")
    public ResUlt getPage(@PathVariable Integer pageNum, @RequestBody AppInfo appInfo){
        System.out.println("222");
        return new ResUlt(true,appInfoService.getPage(appInfo,pageNum),null);
    }
    @RequestMapping("/getApk/{apkName}")
    public ResUlt getApk(@PathVariable String apkName){
        return new ResUlt(appInfoService.getApk(apkName),null,null);
    }
    @RequestMapping("/save")
    public ResUlt getApk(AppInfo appInfo, MultipartFile file, HttpServletRequest request){
        String token = request.getHeader("token");
        DevUser devUser  = (DevUser) SessionUtils.get(token);
        if(file!=null && !file.isEmpty()){
            String realPath = request.getServletContext().getRealPath("/image");
            File file1 = new File(realPath);
            if (file1.exists()){
                file1.mkdirs();
            }
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (substring.equalsIgnoreCase(".jpg")) {
                if (file.getSize()<500000) {
                    String s = UUID.randomUUID().toString().replace("-", "") + substring;
                    appInfo.setLogopicpath(s);
                    file1 = new File(realPath + "\\" + s);
                    try {
                        file.transferTo(file1);
                        if (devUser.getId() != null) {
                            appInfo.setDevid(devUser.getId());
                            appInfo.setStatus(1L);
                            appInfo.setUpdatedate(new Date());
                            appInfo.setModifyby(devUser.getId());
                            appInfo.setModifydate(new Date());
                        } else {
                            appInfo.setStatus(1L);
                            appInfo.setDownloads(0L);
                            appInfo.setCreatedby(devUser.getId());
                            appInfo.setCreationdate(new Date());
                        }
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    return new ResUlt(false, null, "图片大小不能小于500k");
                }
            }else {
                return new ResUlt(false, null, "图片格式不正确");
            }
        }else if (devUser.getId()==null){
            return new ResUlt(false, null, "请选择图片");
        }
        return new ResUlt(appInfoService.save(appInfo), null, null);
    }
    @RequestMapping("/getAppInfo/{id}")
    public ResUlt getAppInfo(@PathVariable Long id){
        return new ResUlt(true,appInfoService.getById(id),null);
    }
    @RequestMapping("/del/{id}")
    public ResUlt del(@PathVariable Long id,HttpServletRequest request){
        String realPath = request.getServletContext().getRealPath("/image");
        String apk = request.getServletContext().getRealPath("/apk");
        File file = new File(realPath);
        if (file.exists()){
            AppInfo byId = appInfoService.getById(id);
            if (byId.getLogopicpath()!=null){
               File file1 = new File(realPath+"\\"+byId.getLogopicpath());
               if (file1.exists()){
                    file1.delete();
               }
            }
        }
        file =new File(apk);
        if (file.exists()){
            List<AppVersion> appInfo = appVersionService.getAppInfo(id);
            appInfo.forEach(appVersion -> {
                if (appVersion.getApklocpath()!=null){
                    File file1 = new File(apk+"\\"+appVersion.getApklocpath());
                    if (file1.exists()){
                        file1.delete();
                    }
                }
            });
        }
        return new ResUlt(appInfoService.del(id),null,null);
    }
    @RequestMapping("/onUp/{id}/{status}")
    public ResUlt onUp(@PathVariable Long id,@PathVariable Integer status){
        return new ResUlt(appInfoService.onUp(id,status),null,null);
    }
}
