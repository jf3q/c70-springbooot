package com.itheima.com.controller;


import com.itheima.com.controller.result.ResUlt;
import com.itheima.com.dto.LoginDto;
import com.itheima.com.service.LoginService;
import com.itheima.com.uitls.SessionUtils;
import com.itheima.com.vo.LoginUserVo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/user")
public class LoginController {
    @Resource
    LoginService loginService;
    @PostMapping("/loginUser")
    public ResUlt login(@RequestBody LoginDto loginDto){
        LoginUserVo login = loginService.login(loginDto);
        if (login.getAccount()==null){
            return new ResUlt(false,null,"密码账号错误");
        }
        return new ResUlt(true,login,"登入成功");
    }
    @RequestMapping("/out")
    public ResUlt out(HttpServletRequest request){
        String token = request.getHeader("token");
        if (SessionUtils.get(token)!=null){
            SessionUtils.del(token);
            return new ResUlt(true,null,"注销成功");
        }
        return new ResUlt(false,null,"注销失败");
    }
}































