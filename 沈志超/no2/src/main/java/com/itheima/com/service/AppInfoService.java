package com.itheima.com.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.com.dao.AppInfoDao;
import com.itheima.com.entity.AppInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AppInfoService {
    @Autowired
    AppInfoDao appInfoDao;
    public Object getPage(AppInfo appInfo, Integer pageNum) {
        PageHelper.startPage(pageNum,5,"id desc");
        List<AppInfo> appInfos = appInfoDao.queryAll(appInfo);
        return new PageInfo<>(appInfos);
    }

    public Boolean getApk(String apkName) {
        return appInfoDao.selectByApkname(apkName)==0;
    }

    public boolean save(AppInfo appInfo) {
        if (appInfo.getId()!=null){
            return appInfoDao.update(appInfo)>0;
        }else {
            return appInfoDao.insert(appInfo)>0;
        }
    }

    public AppInfo getById(Long id) {
        return appInfoDao.queryById(id);
    }

    public boolean del(Long id) {
        return appInfoDao.deleteById(id)>0;
    }

    public boolean onUp(Long id, Integer status) {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(id);
        if (status==4){
            appInfo.setStatus(4L);
            appInfo.setCreationdate(new Date());
        }else if (status ==5){
            appInfo.setStatus(5L);
            appInfo.setCreationdate(new Date());
        }else if (status==2){
            appInfo.setStatus(2L);
            appInfo.setCreationdate(new Date());
        }else if (status==3){
            appInfo.setStatus(3L);
            appInfo.setCreationdate(new Date());
        }
        return appInfoDao.update(appInfo)>0;
    }
}
