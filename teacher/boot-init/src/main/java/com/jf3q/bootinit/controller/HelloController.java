package com.jf3q.bootinit.controller;

import com.jf3q.bootinit.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:xiaojie
 * @create: 2023-12-29 14:39
 * @Description:
 */
@RestController
public class HelloController {

    @Value("${user.phone}")
    String username;
    @Autowired
    UserInfo userInfo;
    @GetMapping("/hello")
    public String hello(){
        return "hello "+userInfo;
    }
}
