package com.jf3q.bootinit.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author:xiaojie
 * @create: 2023-12-29 15:07
 * @Description:
 */
@Data
@Component
@ConfigurationProperties(prefix = "user")
public class UserInfo {
    private String phone;
    private String age;
}
