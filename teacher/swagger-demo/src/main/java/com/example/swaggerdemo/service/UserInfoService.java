package com.example.swaggerdemo.service;

import com.example.swaggerdemo.entity.UserInfo;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author:xiaojie
 * @create: 2024-01-04 09:41
 * @Description:
 */
@Service
public class UserInfoService {
    public UserInfo getUser(Integer id) {

        return new UserInfo(1,"xiaojie","124");
    }

    public List<UserInfo> getList() {

        return Arrays.asList(new UserInfo[]{
                new UserInfo(1,"zhangsan","123"),
                new UserInfo(2,"lisi","123"),
                new UserInfo(3,"wangwu","123"),
        });
    }
}
