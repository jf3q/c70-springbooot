package com.example.swaggerdemo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo<T> {

    public String code;
    public String mess;
    public T data;

    public static<T> ResultVo<T> success(String mess, T data){
        return new ResultVo<T>("200",mess,data);
    }

    public static<T> ResultVo error(String mess){
        return new ResultVo<T>("500",mess,null);
    }
}
