package com.jf3q.securitydemo.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class ValidCodeFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //下面代码表示只过滤/login
        if ("POST".equalsIgnoreCase(request.getMethod()) && "/login".equals(request.getServletPath())) {
            String requestCode = request.getParameter("code"); //从前端获取用户填写的验证码
            String validcode = (String) request.getSession().getAttribute("validcode");
            if (!validcode.toLowerCase().equals(requestCode.toLowerCase())) { // 验证码不相同就跳转
                //手动设置异常
                request.getSession().setAttribute("error","验证码错误");//存储错误信息，以便前端展示
                response.sendRedirect("/toLogin");
                 return;//不让走后面的放行代码
            }else{
                request.getSession().removeAttribute("error");
            }
        }
        filterChain.doFilter(request, response);//验证码相同放行
    }


}
