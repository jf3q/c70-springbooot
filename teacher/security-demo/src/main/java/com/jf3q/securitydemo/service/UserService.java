package com.jf3q.securitydemo.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jf3q.securitydemo.dao.UserMapper;
import com.jf3q.securitydemo.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:xiaojie
 * @create: 2024-01-29 14:10
 * @Description:
 */
@Service
public class UserService extends ServiceImpl<UserMapper, UserInfo> implements UserDetailsService {
    @Autowired
    UserMapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        LambdaQueryWrapper<UserInfo> queryWrapper= new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getUname,username);
        List<UserInfo> list = userMapper.selectList(queryWrapper);
        if (list==null || list.size()==0) {
            throw  new UsernameNotFoundException("账号不存在");
        }
        UserInfo userInfo = list.get(0);//剩下一个关键属性roles没赋值


        userInfo.setRoles(userMapper.getRolesByUserId(userInfo.getId()));

        return userInfo;
    }
}
