package com.jf3q.securitydemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jf3q.securitydemo.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu>  {

    List<Menu> getAll();
}
