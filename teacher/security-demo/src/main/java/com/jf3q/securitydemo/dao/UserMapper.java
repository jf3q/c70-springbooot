package com.jf3q.securitydemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jf3q.securitydemo.entity.Role;
import com.jf3q.securitydemo.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<UserInfo> {


    //为roles赋值

    List<Role> getRolesByUserId(Integer id);

}
