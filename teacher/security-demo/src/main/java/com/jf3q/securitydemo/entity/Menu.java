package com.jf3q.securitydemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "menu")
public class Menu {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String mpath;
    private String mcode;

    @TableField(exist = false)
    private List<Role> roles;
}
