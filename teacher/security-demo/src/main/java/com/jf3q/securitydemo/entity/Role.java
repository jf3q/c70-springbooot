package com.jf3q.securitydemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "role")
public class Role {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String roleName;

    private String roleCode;
    private List<Menu> menus;//该角色能访问的菜单有哪些
}
