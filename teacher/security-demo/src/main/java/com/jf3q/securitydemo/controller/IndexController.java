package com.jf3q.securitydemo.controller;

import com.jf3q.securitydemo.utils.ValidCode;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author:xiaojie
 * @create: 2024-01-28 08:22
 * @Description:
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String  index(){
        return "index";
    }
    //level1/1
    @GetMapping("/level{num1}/{num2}")
    public String level(@PathVariable Integer num1,@PathVariable Integer num2){

        return "pages/level"+num1+"/"+num2;
    }

    @GetMapping("/loginError")
    public String error(Model model){
        model.addAttribute("error","账号密码错误");
        return "pages/login";
    }


    //怎么拿到你的验证码  toLogin的时候  也就是login页面的时候
    @GetMapping("/validcode")
    public void getValidPicture(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ValidCode validCode = new ValidCode();
        BufferedImage image = validCode.getImage();
        String validcode = validCode.getValidcode();//获取随机验证码(字符串)
        System.out.println("validcode:"+validcode);
        HttpSession session = request.getSession();
        session.setAttribute("validcode", validcode);//将随机验证码存入session
        validCode.output(image, response.getOutputStream());//输出图片
    }

}
