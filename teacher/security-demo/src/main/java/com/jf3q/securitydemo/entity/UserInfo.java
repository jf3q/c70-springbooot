package com.jf3q.securitydemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import net.sf.jsqlparser.statement.merge.Merge;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@TableName("userinfo")
public class UserInfo implements UserDetails {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String uname;
    private String pwd;

    @TableField(exist = false)
    private List<Role> roles;//角色  权限


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> list= new ArrayList<>();
        for (Role role : roles) {
            SimpleGrantedAuthority simpleGrantedAuthority= new SimpleGrantedAuthority(role.getRoleCode());
            list.add(simpleGrantedAuthority);
            List<Menu> menus = role.getMenus();
            for (Menu menu : menus) {
                SimpleGrantedAuthority authority= new SimpleGrantedAuthority(menu.getMcode());
                list.add(authority);
            }

        }

        return list;
    }

    @Override
    public String getPassword() {
        return pwd;
    }

    @Override
    public String getUsername() {
        return uname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
