package com.jf3q.thymeleafdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author:xiaojie
 * @create: 2024-01-04 08:47
 * @Description:
 */
@Data
@AllArgsConstructor
public class UserInfo {

    private Integer id;
    private String username;
    private String pass;
}
