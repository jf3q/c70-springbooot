package com.jf3q.thymeleafdemo.controller;

import com.jf3q.thymeleafdemo.entity.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author:xiaojie
 * @create: 2024-01-04 08:36
 * @Description:
 */

@Controller
public class ThymeleafController {

//    @PostMapping("/login")
//    public String login(UserInfo userInfo){
//        System.out.println("exec login()"+userInfo);
//        return "index";
//    }


    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("username","xiaojie");
        model.addAttribute("username2","<h1>xiaojie</h1>");
        model.addAttribute("sex",1);
        model.addAttribute("city","taiyuan");
        model.addAttribute("nowTime",new Date());
        List<UserInfo> users= Arrays.asList(
                new UserInfo[]{
                        new UserInfo(1,"李白","123"),
                        new UserInfo(2,"杜甫","123"),
                        new UserInfo(3,"白居易","123")
                }
        );
        model.addAttribute("users",users);
        return "index";
    }
}
