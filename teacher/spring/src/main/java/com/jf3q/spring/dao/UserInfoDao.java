package com.jf3q.spring.dao;

import com.jf3q.spring.entity.UserInfo;

public interface UserInfoDao  {

    void insert(UserInfo userInfo);
}
