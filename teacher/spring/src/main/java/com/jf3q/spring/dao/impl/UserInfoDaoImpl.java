package com.jf3q.spring.dao.impl;

import com.jf3q.spring.dao.UserInfoDao;
import com.jf3q.spring.entity.UserInfo;
import org.springframework.stereotype.Repository;

@Repository
public class UserInfoDaoImpl implements UserInfoDao {
    @Override
    public void insert(UserInfo userInfo) {
        System.out.println("新增成功："+userInfo.getUsername());
    }
}
