package com.jf3q.spring.config;

import com.jf3q.spring.dao.impl.UserInfoDaoImpl;
import com.jf3q.spring.service.impl.UserInfoServiceImpl;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

//<context:component-scan base-package="com.jf3q.spring.service,com.jf3q.spring.dao"/>
@Configuration// applicationContext.xml
@ComponentScan(basePackages = {"com.jf3q.spring.controller","com.jf3q.spring.service","com.jf3q.spring.dao"})
@EnableWebMvc//相当于<mvc:annotation-driven/>
public class SpringConf  implements WebMvcConfigurer {


    //静态资源放行  <mvc:default-servlet-handler/>
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    //视图解析器
    @Bean
    InternalResourceViewResolver internalResourceViewResolver(){
        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();

        internalResourceViewResolver.setSuffix(".jsp");
        internalResourceViewResolver.setPrefix("/");
        return internalResourceViewResolver;
    }


    // <bean class="org.apache.commons.dbcp.BasicDataSource" id="dataSource">
    //        <property name="driverClassName" value=""/>
    //    </bean>
//    @Bean
//    BasicDataSource dataSource(){
//        BasicDataSource basicDataSource = new BasicDataSource();
//        basicDataSource.setDriverClassName("com.mysql.cj.jdbc.");
//        basicDataSource.setUrl("com.mysql.cj.jdbc.");
//        basicDataSource.setUsername("com.mysql.cj.jdbc.");
//        return basicDataSource;
//    }



}
