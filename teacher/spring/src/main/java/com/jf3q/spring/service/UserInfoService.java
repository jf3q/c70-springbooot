package com.jf3q.spring.service;

import com.jf3q.spring.entity.UserInfo;

public interface UserInfoService  {
    void addUser(UserInfo userInfo);
}
