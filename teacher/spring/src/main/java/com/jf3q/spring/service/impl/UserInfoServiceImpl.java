package com.jf3q.spring.service.impl;

import com.jf3q.spring.dao.UserInfoDao;
import com.jf3q.spring.entity.UserInfo;
import com.jf3q.spring.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;


    @Override
    public void addUser(UserInfo userInfo) {
        userInfoDao.insert(userInfo);
    }
}
