package com.jf3q.spring.service;

import com.jf3q.spring.config.SpringConf;
import com.jf3q.spring.entity.UserInfo;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class UserInfoServiceTest {

    @Test
    public void addUser() {

//        ApplicationContext app= new ClassPathXmlApplicationContext("applicationContext.xml");
        ApplicationContext app= new AnnotationConfigApplicationContext(SpringConf.class);
        UserInfoService service = app.getBean(UserInfoService.class);
        service.addUser(new UserInfo("lisi"));
    }
}