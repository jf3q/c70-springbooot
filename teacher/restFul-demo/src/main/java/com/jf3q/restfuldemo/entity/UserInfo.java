package com.jf3q.restfuldemo.entity;

import lombok.Data;

/**
 * @author:xiaojie
 * @create: 2024-01-02 13:47
 * @Description:
 */
@Data
public class UserInfo {
    private String username;
    private Integer id;
}
