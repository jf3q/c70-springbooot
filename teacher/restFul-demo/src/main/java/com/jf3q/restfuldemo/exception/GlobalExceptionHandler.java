package com.jf3q.restfuldemo.exception;

import com.jf3q.restfuldemo.vo.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author:xiaojie
 * @create: 2024-01-02 14:43
 * @Description:
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    public ResultVo nullP(){
        return ResultVo.error("空指针异常");
    }
    @ExceptionHandler(ArithmeticException.class)
    public ResultVo aP(ArithmeticException e){
        return ResultVo.error(e.getMessage());
    }
    @ExceptionHandler(Exception.class)
    public ResultVo aP(Exception e){
        return ResultVo.error(e.getMessage());
    }
}
