package com.jf3q.restfuldemo.controller;

import com.jf3q.restfuldemo.entity.UserInfo;
import com.jf3q.restfuldemo.vo.ResultVo;
import org.springframework.web.bind.annotation.*;

/**
 * @author:xiaojie
 * @create: 2024-01-02 13:46
 * @Description:
 */

@RestController
@RequestMapping("/rest")
public class RestFulController {

    @PostMapping
    public ResultVo addUser(@RequestBody UserInfo userInfo){
        System.out.println("新增用户信息："+userInfo);
        return ResultVo.success("新增用户信息success",userInfo);
    }
    @DeleteMapping("/{id}")
    public String del(@PathVariable Integer id){
        System.out.println("删除用户ID："+id);
        return "删除用户成功";
    }
    @PutMapping
    public String update(@RequestBody UserInfo userInfo){
        System.out.println("修改后");
        return "修改用户成功";
    }
    @GetMapping("/{id}")
    public String getUser(@PathVariable Integer id){
        System.out.println("用户ID："+id);
        return "查询用户ID成功！";
    }
    @GetMapping
    public String getUsers(){
        System.out.println("查询所有的用户");
        return "查询所有的用户";
    }
}
