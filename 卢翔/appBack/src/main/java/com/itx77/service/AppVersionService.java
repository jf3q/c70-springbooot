package com.itx77.service;

import com.itx77.dao.AppInfoDao;
import com.itx77.dao.AppVersionDao;
import com.itx77.entity.AppInfo;
import com.itx77.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: .x77
 * @CreateTime:2023/12/9 10:19
 */
@Service
public class AppVersionService {

    @Autowired
    AppVersionDao appVersionDao;
    @Autowired
    AppInfoDao appInfoDao;


    public List<AppVersion> selByAppId(Long appId) {
        List<AppVersion> appVersion = appVersionDao.queryByAppId(appId);
        return appVersion;
    }

    public void addVersion(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
        AppInfo appInfo = new AppInfo();
        appInfo.setVersionid(appVersion.getId());
        appInfo.setId(appVersion.getAppid());
        appInfoDao.update(appInfo);
    }
}
