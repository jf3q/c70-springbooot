package com.itx77.service;

import com.itx77.dao.BackendUserDao;
import com.itx77.dao.DevUserDao;
import com.itx77.dto.LoginDto;
import com.itx77.entity.BackendUser;
import com.itx77.entity.DevUser;
import com.itx77.utils.SessionUtils;
import com.itx77.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * @author: .x77
 * @CreateTime:2023/12/7 10:57
 */
@Service
public class DevUserService {
    @Autowired
    DevUserDao devUserDao;
    @Autowired
    BackendUserDao backendUserDao;

    public LoginVo login(LoginDto loginDto) {

        if (loginDto.getUserType() == 1){
            BackendUser backendUser = new BackendUser();
            backendUser.setUsercode(loginDto.getUserName());
            backendUser.setUserpassword(loginDto.getPassword());
            List<BackendUser> login = backendUserDao.login(backendUser);
            if (login.size() == 0 ){
                throw new RuntimeException("账号或密码错误");
            }
            LoginVo loginVo = new LoginVo();
            loginVo.setUserId(login.get(0).getId());
            loginVo.setUserCode(loginDto.getUserName());
            loginVo.setUserType("admin");
            loginVo.setUserName(login.get(0).getUsername());
            StringBuffer token = new StringBuffer();
            String uuid = UUID.randomUUID().toString().replace("-", "");
            token.append(uuid+"-");
            token.append(loginVo.getUserCode()+"-");
            token.append(loginVo.getUserId()+"-");
            token.append(System.currentTimeMillis()+"-");
            token.append(loginVo.getUserType());
            loginVo.setToken(token.toString());
            SessionUtils.put(token.toString(),login.get(0));
            return loginVo;
            //todo admin登录
        }else if (loginDto.getUserType() == 2){
            DevUser devUser = new DevUser();
            devUser.setDevcode(loginDto.getUserName());
            devUser.setDevpassword(loginDto.getPassword());
            List<DevUser> login = devUserDao.login(devUser);
            if (login.size() == 0){
                throw new RuntimeException("账号或密码错误");
            }
            LoginVo loginVo = new LoginVo();
            loginVo.setUserId(login.get(0).getId());
            loginVo.setUserCode(loginDto.getUserName());
            loginVo.setUserType("dev");
            loginVo.setUserName(login.get(0).getDevname());
            StringBuffer token = new StringBuffer();
            String uuid = UUID.randomUUID().toString().replace("-", "");
            token.append(uuid+"-");
            token.append(loginVo.getUserCode()+"-");
            token.append(loginVo.getUserId()+"-");
            token.append(System.currentTimeMillis()+"-");
            token.append(loginVo.getUserType());
            loginVo.setToken(token.toString());
            SessionUtils.put(token.toString(),login.get(0));
            return loginVo;
        }else {
            throw new RuntimeException("没有此账号类型");
        }
    }
}
