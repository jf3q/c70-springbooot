package com.itx77.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-11-27 19:39:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class BackendUser implements Serializable {
    private static final long serialVersionUID = -47211084528899375L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户编码
     */
    private String userCode;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    private Long userType;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdBy;
    /**
     * 创建时间
     */
    private Date creationDate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyBy;
    /**
     * 最新更新时间
     */
    private Date modifyDate;
    /**
     * 用户密码
     */
    private String userPassword;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}

