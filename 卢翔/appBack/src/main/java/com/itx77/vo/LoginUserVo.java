package com.itx77.vo;

import lombok.Data;

/**
 * @author: .x77
 * @CreateTime:2023/11/27 14:31
 */
@Data
public class LoginUserVo {
    private String userCode;
    private String userName;
    private String token;
}
