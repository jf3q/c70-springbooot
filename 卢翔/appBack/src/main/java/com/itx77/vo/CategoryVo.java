package com.itx77.vo;

import java.util.List;

/**
 * @author: .x77
 * @CreateTime:2023/12/8 8:56
 */
public class CategoryVo {
    private Long id;
    private String categoryname;
    private Long parentid;

    private List<CategoryVo> categoryList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<CategoryVo> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryVo> categoryList) {
        this.categoryList = categoryList;
    }
}
