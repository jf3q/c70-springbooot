package com.itx77.sys;
public class SysConstant {
    public static class UserTypeStr{
        public final static String admin="admin";
        public final static String dev="dev";
    }
    public static class UserTypeInt{
        public final static Integer admin= 1;
        public final static Integer dev= 2;
    }
    public static class VersionStatus{
        public final static Long no= 1L;
        public final static Long yes= 2L;
        public final static Long per= 3L;
    }
}