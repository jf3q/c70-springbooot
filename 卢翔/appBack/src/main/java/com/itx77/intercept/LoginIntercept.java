package com.itx77.intercept;

import com.itx77.utils.SessionUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;


/**
 * @author: .x77
 * @CreateTime:2023/11/28 10:28
 */
@Component
public class LoginIntercept implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        String requestURI = request.getRequestURI();
        if (token ==null){
            response.setStatus(401);
        }else if (SessionUtils.get(token) == null){
            response.setStatus(403);
        }else {
            String[] split = token.split("-");
            String createTime = split[2];
            if (requestURI.equals("/appInfo/review")){
                //判断角色
                Integer s = Integer.valueOf(split[3]);
                if (s == 2){
                    response.setStatus(403);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
