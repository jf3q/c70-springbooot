package com.itx77.config;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInterceptor;
import com.itx77.intercept.LoginIntercept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: .x77
 * @CreateTime:2023/12/18 15:52
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    LoginIntercept loginIntercept;
        @Override
        public void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(loginIntercept).addPathPatterns("/**").excludePathPatterns("/login","/upload/**");
        }

    @Bean
    public PageInterceptor pageInterceptor(){
        return new PageInterceptor();
    }
}
