package com.itx77.dto;

/**
 * @author: .x77
 * @CreateTime:2023/12/7 10:53
 */
public class LoginDto {
    private String userName;
    private String password;
    private Integer userType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
