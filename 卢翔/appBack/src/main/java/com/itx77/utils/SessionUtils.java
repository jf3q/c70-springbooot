package com.itx77.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: .x77
 * @CreateTime:2023/11/27 14:58
 */
public class SessionUtils {
    static Map<String, Object> map = new HashMap<>();
    public static void put(String token,Object loginUser){
        map.put(token,loginUser);
    }
    public static Object get(String token){
        return map.get(token);
    }
    public static void remove(String token){
         map.remove(token);
    }
}
