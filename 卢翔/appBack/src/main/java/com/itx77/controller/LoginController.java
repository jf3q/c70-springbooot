package com.itx77.controller;

import com.itx77.dto.LoginDto;
import com.itx77.service.DevUserService;
import com.itx77.utils.SessionUtils;
import com.itx77.vo.LoginVo;
import com.itx77.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import jakarta.servlet.http.HttpServletRequest;

/**
 * @author: .x77
 * @CreateTime:2023/12/6 15:52
 */
@RestController
public class LoginController {
    @Autowired
    DevUserService devUserService;

    @PostMapping("/login")
    public ResultVo getLogin(@RequestBody LoginDto loginDto){
        LoginVo loginVo = null;
        try {
            loginVo=  devUserService.login(loginDto);

        } catch (Exception e) {
           e.printStackTrace();
           return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功",loginVo);
    }

    @PostMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("注销成功！");
    }
}
