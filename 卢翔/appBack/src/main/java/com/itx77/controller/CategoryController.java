package com.itx77.controller;

import com.itx77.service.CategoryService;
import com.itx77.vo.CategoryVo;
import com.itx77.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: .x77
 * @CreateTime:2023/12/8 8:53
 */
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @GetMapping("/tree")
    public ResultVo Tree(){
        CategoryVo tree = categoryService.Tree();
        return ResultVo.success(null,tree);
    }
}
