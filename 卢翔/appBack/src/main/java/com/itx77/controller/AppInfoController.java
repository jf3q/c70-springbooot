package com.itx77.controller;

import com.github.pagehelper.PageInfo;
import com.itx77.entity.AppInfo;
import com.itx77.entity.DevUser;
import com.itx77.service.AppInfoService;
import com.itx77.utils.SessionUtils;
import com.itx77.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author: .x77
 * @CreateTime:2023/12/7 15:15
 */
@RestController
@RequestMapping("/appInfo")
public class AppInfoController {

    @Autowired
    AppInfoService appInfoService;

    @Value("${web.upload-path}")
    private String uploadPath;

    @PostMapping("/getPage")
    public ResultVo getPage(@RequestBody AppInfo appInfo, @RequestParam(defaultValue = "1") Integer pageNum,HttpServletRequest request){
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String s = split[4];
        if (s.equals("dev")){
            Long user = Long.valueOf(split[2]);
            appInfo.setDevid(user);
        }
        PageInfo page = appInfoService.getPage(appInfo, pageNum);
        return ResultVo.success(null,page);
    }


    @PostMapping("/insAppInfo")
    public ResultVo insAppInfo(AppInfo appInfo , MultipartFile file, HttpServletRequest request){
        if (file != null){
            String filename = file.getOriginalFilename();
            String extension = FilenameUtils.getExtension(filename);
            if (file.getSize() > 1024 * 520){
                ResultVo.error("图片的大小超过5M");
            }else if (extension.equalsIgnoreCase("jpg")||
                    extension.equalsIgnoreCase("png")||
                    extension.equalsIgnoreCase("jpeg")||
                    extension.equalsIgnoreCase("gif")){
                String replace = UUID.randomUUID().toString().replace("-","");
                File file1 = new File(uploadPath+"upload");
                if (!file1.exists()){
                    file1.mkdirs();
                }
                try {
                    file.transferTo(new File(uploadPath+"upload/"+replace+"."+extension));
                    appInfo.setLogopicpath("upload/"+replace+"."+extension);
                } catch (IOException e) {
                    return ResultVo.error("上传logo图错误");
                }
            }else{
               return ResultVo.error("图片格式错误");
            }
        }else if (appInfo.getId() == null){
               return ResultVo.error("新增必须上传图片");
        }
        String token = request.getHeader("token");
        DevUser devUser = (DevUser) SessionUtils.get(token);
        if (appInfo.getId() == null){
            appInfo.setDevid(devUser.getId());
            appInfo.setStatus(1L);
            appInfo.setDownloads(0L);
            appInfo.setCreatedby(devUser.getId());
            appInfo.setCreationdate(new Date());

        }else if (appInfo.getId() != null){
            appInfo.setModifyby(devUser.getId());
            appInfo.setModifydate(new Date());
        }
        appInfoService.appUpload(appInfo);
        return ResultVo.success("操作成功");
    }

    @GetMapping("/selById")
    public ResultVo selById(Long id){
        AppInfo appInfo = appInfoService.selById(id);
        return ResultVo.success(null,appInfo);
    }

    @PostMapping("/selApkName")
    public ResultVo selApkName(@RequestBody AppInfo appInfo){
        Boolean flag = appInfoService.selByApk(appInfo);
        if (flag){
            return ResultVo.success("当前apk可以使用");
        }else {
            return ResultVo.error("当前apk已被使用");
        }
    }


    @DeleteMapping("/delById")
    public ResultVo delById(Long id,HttpServletRequest request){
         boolean b = appInfoService.delById(id,request);
      if (b){
          return ResultVo.success("删除成功！");
      }
      return  ResultVo.error("删除失败！");
    }

    @PostMapping("/updStatus")
    public ResultVo updStatus(@RequestBody AppInfo appInfo){
        if (appInfo.getStatus() == 2){
            appInfoService.updStatus(appInfo);
            return ResultVo.success("审核通过");
        }else {
            appInfoService.updStatus(appInfo);
            return ResultVo.success("打回成功");
        }
    }


    @GetMapping("/updByXiu")
        public ResultVo updByXiu(Long id){
        appInfoService.updByXiu(id);
        return ResultVo.success("操作成功");
    }
}
