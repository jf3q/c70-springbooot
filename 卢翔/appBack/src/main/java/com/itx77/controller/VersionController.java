package com.itx77.controller;

import com.itx77.entity.AppVersion;
import com.itx77.entity.DevUser;
import com.itx77.service.AppVersionService;
import com.itx77.utils.SessionUtils;
import com.itx77.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import jakarta.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author: .x77
 * @CreateTime:2023/12/11 10:17
 */
@RestController
@RequestMapping("/version")
public class VersionController {
    @Autowired
    AppVersionService appVersionService;
    @GetMapping("/selVersion")
    public ResultVo selVersion(Long appId){
        List<AppVersion> versions = null;
        try {
            versions = appVersionService.selByAppId(appId);
        } catch (Exception e) {
            return ResultVo.error("");
        }
        return ResultVo.success(null,versions);
    }


    @PostMapping("/addVersion")
    public ResultVo addVersion(AppVersion appVersion, MultipartFile apkFile, HttpServletRequest request){
        if (apkFile != null){
            String filename = apkFile.getOriginalFilename();
            String extension = FilenameUtils.getExtension(filename);
            if (apkFile.getSize()>1024*1025*500){
                return ResultVo.error("apk文件不能超过5MB");
            }else if (
                    extension.equalsIgnoreCase("apk")
            ){
                String realPath = request.getServletContext().getRealPath("/upload/apk");
                File file = new File(realPath);
                if (!file.exists()){
                    file.mkdirs();
                }
                String replace = UUID.randomUUID().toString().replace("-", "");
                File save = new File(realPath+"/"+replace+"."+extension);
                appVersion.setDownloadlink("/upload/apk"+"/"+replace+"."+extension);
                try {
                    apkFile.transferTo(save);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }else {
                return ResultVo.error("上传的文件格式不对");
            }
        }else if (apkFile==null){
            return ResultVo.error("新增版本必须上传APK文件");
        }
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String s = split[4];
        System.out.println(s);
        if (s.equals("dev")){
            DevUser devUser = (DevUser) SessionUtils.get(token);
            appVersion.setCreatedby(devUser.getId());
            appVersion.setCreationdate(new Date());
            appVersion.setPublishstatus(1L);
            appVersionService.addVersion(appVersion);
            return ResultVo.success("新增版本成功！");
        }else {
            return ResultVo.error("此操作只能开发人员操作！");
        }
    }
}
