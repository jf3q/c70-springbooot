package com.atww.nucleic_acid.service;

import com.atww.nucleic_acid.entity.Hospital;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 9:18
 * @bug退散
 */
public interface HospitalService {
    List<Hospital> getHospitalList();

}
