package com.atww.nucleic_acid.service.impl;

import com.atww.nucleic_acid.dao.HospitalDao;
import com.atww.nucleic_acid.entity.Hospital;
import com.atww.nucleic_acid.service.HospitalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 9:18
 * @bug退散
 */
@Service
public class HospitalServiceImpl implements HospitalService {
    @Autowired
    private HospitalDao hospitalDao;
    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public List<Hospital> getHospitalList() {
        List<Hospital> list = (List<Hospital>) redisTemplate.opsForValue().get("list");
        if (list==null || list.size()==0){
            List<Hospital> hospitals = hospitalDao.queryAll(new Hospital());
            redisTemplate.opsForValue().set("list",hospitals);
        }
        System.out.println(list);
        return list ;
    }
}
