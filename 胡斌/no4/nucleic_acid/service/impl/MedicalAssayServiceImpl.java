package com.atww.nucleic_acid.service.impl;

import com.atww.nucleic_acid.dao.MedicalAssayDao;
import com.atww.nucleic_acid.entity.MedicalAssay;
import com.atww.nucleic_acid.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 10:31
 * @bug退散
 */
@Service
public class MedicalAssayServiceImpl implements MedicalAssayService {
    @Autowired
    private MedicalAssayDao medicalAssayDao;
    @Override
    public List<MedicalAssay> getMedical(Long hId) {
        MedicalAssay medicalAssay=new MedicalAssay();
        medicalAssay.setHospitalId(hId);
        return medicalAssayDao.queryAll(medicalAssay);
    }

    @Override
    public void addMedical(MedicalAssay medicalAssay) {
         medicalAssayDao.insert(medicalAssay);
    }

    @Override
    public Integer updateStatus(MedicalAssay medicalAssay) {
      return   medicalAssayDao.update(medicalAssay);
    }
}
