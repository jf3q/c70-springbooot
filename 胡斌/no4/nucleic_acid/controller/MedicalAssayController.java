package com.atww.nucleic_acid.controller;

import com.atww.nucleic_acid.entity.MedicalAssay;
import com.atww.nucleic_acid.service.HospitalService;
import com.atww.nucleic_acid.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 10:27
 * @bug退散
 */
@Controller
public class MedicalAssayController {
    @Autowired
    private MedicalAssayService medicalAssayService;
    @GetMapping("getMedical")
    public String medical(Model model,Long hId){
       List<MedicalAssay> list= medicalAssayService.getMedical(hId);
       model.addAttribute("list",list);
       model.addAttribute("hId",hId);
        return "index";
    }
    @PostMapping("addMedical")
    public String addMedical(MedicalAssay medicalAssay){
        medicalAssay.setAssayResult(0);
      medicalAssayService.addMedical(medicalAssay);
        return "index";
    }
    @ResponseBody
    @RequestMapping("/updateStatus/{status}/{id}")
    public Integer test(@PathVariable Integer status,@PathVariable Long id){
        System.out.println(status+"状态:"+"id:"+id);
        Integer result=0;
        if (status==0){
            MedicalAssay medicalAssay=new MedicalAssay();
            medicalAssay.setAssayResult(1);
            medicalAssay.setId(id);
            result=   medicalAssayService.updateStatus(medicalAssay);
        }
        return result;
    }
}
