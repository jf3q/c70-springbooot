package com.atww.nucleic_acid.controller;


import com.atww.nucleic_acid.entity.Hospital;
import com.atww.nucleic_acid.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 9:15
 * @bug退散
 */
@RestController
@RequestMapping("/hospital")
public class HospitalController {
    @Autowired
    private HospitalService hospitalService;
    @GetMapping("")
    public List<Hospital> getHospitalList(){
        return hospitalService.getHospitalList();
    }
}
