package com.atww.nucleic_acid.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2024-01-05 09:00:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 747618987571147383L;
    /**
     * 检测编号
     */
    private Long id;
    /**
     * 被检测人
     */
    private String assayUser;
    /**
     * 检测机构
     */
    private Long hospitalId;
    /**
     * 0:检测中; 1:阳性;2:阴性;
     */
    private Integer assayResult;
    /**
     * 被检测人手机号
     */
    private String phone;
    /**
     * 被检测人身份证号
     */
    private String cardNum;
    /**
     * 检测日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;
    private String hospitalName;




}

