package com.xjq.admin.service;

import com.xjq.common.dao.BackendUserDao;
import com.xjq.common.dto.LoginDto;
import com.xjq.common.entity.BackendUser;
import com.xjq.common.utiles.SessionUtils;
import com.xjq.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AdminLoginService {
    @Autowired
    BackendUserDao backendUserDao;

    public LoginUserVo adminLogin(LoginDto loginDto) {
        BackendUser backendUser=new BackendUser();
        backendUser.setUsercode(loginDto.getUserCode());
        backendUser.setUserpassword(loginDto.getPassword());
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        if (backendUsers==null||backendUsers.size()==0){
            throw new RuntimeException("账号或密码错误");
        }

        LoginUserVo vo=new LoginUserVo();

        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
        stringBuffer.append("-"+backendUsers.get(0).getId());
        stringBuffer.append("-"+"admin");
        stringBuffer.append("-"+System.currentTimeMillis());

        vo.setUserCode(backendUsers.get(0).getUsercode());
        vo.setUserName(backendUsers.get(0).getUsername());
        vo.setUserType("admin");
        vo.setToken(stringBuffer.toString());
        SessionUtils.put(stringBuffer.toString(),backendUsers.get(0));
        return vo;
    }
}
