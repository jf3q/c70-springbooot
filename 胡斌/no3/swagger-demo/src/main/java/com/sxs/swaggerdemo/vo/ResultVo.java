package com.sxs.swaggerdemo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo<T> {
    public String code;
    public String mess;
    public T date;

    public static<T> ResultVo<T> success(String mess,T data){
        return new ResultVo<>("200",mess,data);
    }
    public static<T> ResultVo<T> error(String mess){
        return new ResultVo<>("500",mess,null);
    }
}
