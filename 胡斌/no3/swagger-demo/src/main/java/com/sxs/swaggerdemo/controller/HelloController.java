package com.sxs.swaggerdemo.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "接口", tags = "接口")
public class HelloController {

    @ApiOperation("hello接口")
    @GetMapping("/hello")
    public String h() {
        return "撒谎狗";
    }
}
