package com.sxs.swaggerdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserInfo {
    private Integer id;
    private String username;
    private String password;
}
