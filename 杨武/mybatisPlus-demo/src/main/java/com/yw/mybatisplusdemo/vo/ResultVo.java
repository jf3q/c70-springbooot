package com.yw.mybatisplusdemo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    private Integer code;

    private String mess;
    private T data;

    public static<T> ResultVo success(String mess,T data){
        return new ResultVo(200,mess,data);
    }
    public static<T> ResultVo error(String mess){
        return new ResultVo(500,mess,null);
    }
}
