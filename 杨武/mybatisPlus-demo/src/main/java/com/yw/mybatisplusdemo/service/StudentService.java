package com.yw.mybatisplusdemo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yw.mybatisplusdemo.entity.Student;

public interface StudentService extends IService<Student> {
    Page getPage(String stuName,Integer pageNum);

}
