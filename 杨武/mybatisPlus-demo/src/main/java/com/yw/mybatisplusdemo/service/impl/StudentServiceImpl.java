package com.yw.mybatisplusdemo.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yw.mybatisplusdemo.dao.StudentDao;
import com.yw.mybatisplusdemo.entity.Student;
import com.yw.mybatisplusdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao,Student> implements StudentService {

    @Autowired
    StudentDao studentDao;
    @Override
    public Page getPage(String stuName,Integer pageNum) {
        Page page=new Page(pageNum,3);
        LambdaQueryWrapper<Student>queryWrapper=new LambdaQueryWrapper<>();
        if (StringUtils.hasText(stuName)){
            queryWrapper.like(Student::getStudentname,stuName);
        }
        queryWrapper.orderByDesc(Student::getId);
        return this.page(page,queryWrapper);
    }
}
