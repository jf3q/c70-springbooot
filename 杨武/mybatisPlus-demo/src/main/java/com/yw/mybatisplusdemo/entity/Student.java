package com.yw.mybatisplusdemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "student")
public class Student {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String studentname;
    private String gender;
    private Integer age;
    private String address;

    //以下属性只为展示使用，不保存数据库
    @TableField(exist = false)
    private String className;
}
