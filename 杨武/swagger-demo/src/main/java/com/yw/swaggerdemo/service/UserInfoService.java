package com.yw.swaggerdemo.service;

import com.yw.swaggerdemo.entity.UserInfo;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class UserInfoService {

    public UserInfo getUser(Integer id) {
        return new UserInfo(1,"sxs","123");
    }

    public List<UserInfo> getList() {
        List<UserInfo> user = Arrays.asList(
                new UserInfo[]{
                        new UserInfo(1,"lisi","124"),
                        new UserInfo(2,"liuyuc","222"),
                        new UserInfo(3,"yc","52sion"),
                }
        );
        return user;
    }
}
