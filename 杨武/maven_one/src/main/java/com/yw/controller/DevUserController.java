package com.yw.controller;

import com.yw.dto.LoginUserDto;
import com.yw.service.LoginUserService;
import com.yw.utils.SessionUtils;
import com.yw.vo.LoginUserVo;
import com.yw.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/devUser")
public class DevUserController {

    @Autowired
    LoginUserService loginUserService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto loginUserDto){
        LoginUserVo vo = null;
        try {
            vo = loginUserService.login(loginUserDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功",vo);
    }

    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("退出成功");
    }
}
