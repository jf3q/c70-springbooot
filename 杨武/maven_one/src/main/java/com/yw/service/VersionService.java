package com.yw.service;

import com.yw.dao.AppVersionDao;
import com.yw.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VersionService {

    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> getList(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        return appVersionDao.queryAllBy(appVersion);
    }


    public void saveOrUpdate(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}
