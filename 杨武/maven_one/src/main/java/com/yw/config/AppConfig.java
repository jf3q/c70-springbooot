package com.yw.config;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.github.pagehelper.PageInterceptor;
import com.yw.interceptors.TokenInterceptors;
import org.apache.commons.dbcp.BasicDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;


@Configuration
@ComponentScan(basePackages = {"com.yw.service", "com.yw.dao", "com.yw.controller"})
@EnableWebMvc
public class AppConfig implements WebMvcConfigurer {
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        StringHttpMessageConverter httpMessageConverter = new StringHttpMessageConverter();
        httpMessageConverter.setDefaultCharset(Charset.forName("utf-8"));
        httpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        FastJsonHttpMessageConverter fastJsonHttpMessageConverter= new FastJsonHttpMessageConverter();
        fastJsonHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        converters.add(httpMessageConverter);
        converters.add(fastJsonHttpMessageConverter);
    }

    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public InternalResourceViewResolver  internalResourceViewResolver(){
        InternalResourceViewResolver in = new InternalResourceViewResolver();
        in.setPrefix("/");
        in.setSuffix(".jsp");
        return in;
    }

    @Bean
    public BasicDataSource dataSource(){
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql:///beyond_platform_db?serverTimezone=Asia/Shanghai&amp;characterEncoding=utf-8");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }

    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(){
        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(dataSource());
        sqlSessionFactory.setPlugins(interceptor());
        return sqlSessionFactory;
    }

    @Bean
    public PageInterceptor interceptor(){
        return new PageInterceptor();
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer(){
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setBasePackage("com.ithemou.dao");
        return mapperScannerConfigurer;
    }

    @Bean
    public CommonsMultipartResolver multipartResolve(){
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("utf-8");
        multipartResolver.setMaxInMemorySize(500000000);
        return multipartResolver;
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TokenInterceptors())
                .addPathPatterns("/**")
                .excludePathPatterns("/devUser/login")
                .excludePathPatterns("/upload/**");
    }

}
