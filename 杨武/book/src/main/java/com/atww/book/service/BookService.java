package com.atww.book.service;

import com.atww.book.entity.Book;
import com.github.pagehelper.PageInfo;


public interface BookService {
    PageInfo<Book> getPage(Integer pageNum, Book book);

    void addBook(Book book);

    void delMany(String[] ids);
}
