package com.atww.book.service.impl;

import com.atww.book.service.BookService;
import com.atww.book.dao.BookDao;
import com.atww.book.entity.Book;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookDao bookDao;

    @Override
    public PageInfo<Book> getPage(Integer pageNum, Book book) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> books = bookDao.queryAll(book);
        return new PageInfo<Book>(books);
    }

    @Override
    public void addBook(Book book) {
        bookDao.insert(book);
    }

    @Override
    public void delMany(String[] ids) {
        bookDao.BookDelMany(ids);
    }
}
