package com.yw.controller;

import com.github.pagehelper.PageInfo;
import com.yw.entity.Hospital;
import com.yw.entity.MedicalAssay;
import com.yw.service.HospitalService;
import com.yw.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/index")
public class MedicalAssayController {

    @Autowired
    MedicalAssayService medicalAssayService;

    @Autowired
    HospitalService hospitalService;

    @GetMapping
    public String getAll(Integer hospitalId, @RequestParam(defaultValue = "1") Integer pageNum, Model model){
        PageInfo<MedicalAssay> pageInfo = medicalAssayService.getAll(hospitalId,pageNum);
        List<Hospital> hospitalList = hospitalService.getList();
        model.addAttribute("page",pageInfo);
        model.addAttribute("hospitalList",hospitalList);
        model.addAttribute("hospitalId",hospitalId);
        return "index";
    }

    @GetMapping("/yes")
    public String getYesNo(Integer id){
        medicalAssayService.updateAssayResult(id);
        return "redirect:/index";
    }

    @GetMapping("/toAdd")
    public String add(Model model){
        List<Hospital> hospitalList = hospitalService.getList();
        model.addAttribute("hospitalList",hospitalList);
        return "add";
    }

    @PostMapping("/add")
    public String add(MedicalAssay medicalAssay){
        medicalAssay.setAssayResult(0);
        medicalAssayService.insert(medicalAssay);
        return "redirect:/index";
    }

}
