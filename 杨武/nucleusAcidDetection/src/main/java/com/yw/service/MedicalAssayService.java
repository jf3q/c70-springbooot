package com.yw.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yw.dao.MedicalAssayDao;
import com.yw.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayService {

    @Autowired
    MedicalAssayDao medicalAssayDao;

    public PageInfo<MedicalAssay> getAll(Integer hospitalId,Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<MedicalAssay> list = medicalAssayDao.queryAllBy(new MedicalAssay().setHospitalId(hospitalId));
        return new PageInfo<>(list);
    }

    public void updateAssayResult(Integer id) {
        medicalAssayDao.update(new MedicalAssay().setAssayResult(1).setId(id));
    }

    public void insert(MedicalAssay medicalAssay) {
        medicalAssayDao.insert(medicalAssay);
    }
}
