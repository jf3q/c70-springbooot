package com.yw.service;

import com.yw.dao.HospitalDao;
import com.yw.entity.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalService {

    @Autowired
    HospitalDao hospitalDao;


    public List<Hospital> getList() {
        return hospitalDao.queryAllBy(new Hospital());
    }
}
