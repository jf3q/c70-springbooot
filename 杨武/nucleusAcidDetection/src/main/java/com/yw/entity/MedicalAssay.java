package com.yw.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-27 15:23:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class MedicalAssay {

    private Integer id;

    private String assayUser;
    /**
     * 对应检测机构表
     */
    private Integer hospitalId;
    /**
     * 0.检测中；1.确诊
     */
    private Integer assayResult;

    private String phone;

    private String cardNum;
    /**
     * yyyy-MM-dd
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;

    private String name;
}

