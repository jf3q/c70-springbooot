package com.yw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-27 15:23:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hospital {
    /**
     * 检测机构编号
     */
    private Integer id;
    /**
     * 检测机构名称
     */
    private String name;
}

