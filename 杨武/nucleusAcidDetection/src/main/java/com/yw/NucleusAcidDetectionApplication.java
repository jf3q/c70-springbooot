package com.yw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.ithemou.dao")
public class  NucleusAcidDetectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(NucleusAcidDetectionApplication.class, args);
    }

}
