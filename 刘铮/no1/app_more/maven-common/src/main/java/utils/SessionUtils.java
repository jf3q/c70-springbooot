package utils;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {

    static Map map = new HashMap();

    public static void setToken(String token, Object user) {
        map.put(token, user);
    }


    public static void removeToken(String token) {
        map.remove("token");
    }


    public static Boolean validate(String token) {
        String[] split = token.split("-");
        Object o = map.get(token);
        if (o == null) {
            return false;
        }
        if (System.currentTimeMillis() - Long.valueOf(split[4]) > 3600 * 1000 * 2) {
            return false;
        }
        return true;
    }


}
