package vo;

public class ResoutVo {

    private String code;
    private String message;
    private Object data;


    public static ResoutVo success(String message, Object data) {
        return new ResoutVo("2000", message, data);
    }


    public static ResoutVo success(Object data) {
        return new ResoutVo("2000", null, data);
    }

    public static ResoutVo error(String message) {
        return new ResoutVo("5000", message, null);
    }


    public ResoutVo() {
    }

    public ResoutVo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
