package com.yangheng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.yangheng.dao")
public class No4Application {

    public static void main(String[] args) {
        SpringApplication.run(No4Application.class, args);
    }

}
