package com.yangheng.intercept;

import com.yangheng.sys.SessionManager;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginIntercept implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //拦截我们的请求 首先拿到我们的token值
        String token = request.getHeader("Token");
        //拿到我们的请求路径  后面管理员操作需要
        String requestURI = request.getRequestURI();
        //判断我们的token值是否存在
        if (token==null){
            //如果不存在就把响应状态值改为401
            response.setStatus(401);
            //判断token值是否伪造进行后端校验
        }else if (SessionManager.get(token)==null){
            //如果不存在就把响应状态值改为403
            response.setStatus(403);
        }else {
            //进行时间判断 是否在有效期
            String[] split = token.split("-");
            //截取我们的token值中的时间
            String createTime = split[1];
            //拿取我们的时间 减去当前时间 是否超过有效期
            if (System.currentTimeMillis()-Long.valueOf(createTime)>60*5*10000){
                //如果超过有效期 就改状态值
                response.setStatus(403);
                //并且移除此token
                SessionManager.remover(token);
            }
            //拦截管理员请求 禁止非管理员操作
            if (request.getRequestURI().equals("/auditor")){
                if (token.split("-")[2].equals("1")){
                    return false;
                }
            }
            //如果上面都存在 没问题就放行
            return true;
        }
        //有问题就不放行
        return false ;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
