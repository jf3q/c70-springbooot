package com.demo1.vo;

import java.util.List;

public class Three {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<Three> chlien;



    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }



    public List<Three> getChlien() {
        return chlien;
    }

    public void setChlien(List<Three> chlien) {
        this.chlien = chlien;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Three{" +
                "id=" + id +
                ", categoryname='" + categoryname + '\'' +
                ", parentid=" + parentid +
                ", chlien=" + chlien +
                '}';
    }
}
