package com.demo1.service;



import com.demo1.entity.AppCategory;

import java.util.List;

public interface AppCategoryservice {
    List<AppCategory> queryById(int id);
    List<AppCategory> queryAllByLimit(AppCategory appCategory);

}
