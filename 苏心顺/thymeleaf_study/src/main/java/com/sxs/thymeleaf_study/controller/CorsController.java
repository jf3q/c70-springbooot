package com.sxs.thymeleaf_study.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CorsController {
    @GetMapping("/getMsg")
    public String getMsg(){
        return "GET success";
    }
    @DeleteMapping("/delMsg")
    public String delMsg(){
        return "delete success";
    }
}
