package com.sxs.thymeleaf_study.config;

import com.sxs.thymeleaf_study.vo.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NullPointerException.class) //@ExceptionHandler 局部异常
    public ResultVo exception(NullPointerException exception) {
        return new ResultVo(200,"成功",null);
    }

    @ExceptionHandler(IndexOutOfBoundsException.class)
    public ResultVo exception(IndexOutOfBoundsException exception) {
        return new ResultVo(200,"成功",null);
    }

    @ExceptionHandler(Exception.class)
    public ResultVo exception(Exception exception) {
        return new ResultVo(200,"成功",null);
    }
}
