package com.sxs.app.intercept;

import java.util.HashMap;
import java.util.Map;

/**
 *存token令牌
 */
public class SeesionUtil {
    private static Map<String,Object> map = new HashMap<>();

    //把token存起来
    public static void put(String token,Object data){
        map.put(token, data);
    }
    public static Object get(String token){
        return map.get(token);
    }

    public static void removeToken(String token){
         map.remove(token);
    }
}
