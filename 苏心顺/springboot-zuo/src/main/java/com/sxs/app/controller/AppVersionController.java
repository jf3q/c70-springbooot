package com.sxs.app.controller;


import com.sxs.app.entity.AppInfo;
import com.sxs.app.entity.AppVersion;
import com.sxs.app.entity.DevUser;
import com.sxs.app.intercept.SeesionUtil;
import com.sxs.app.service.AppVersionService;
import com.sxs.app.service.AppinfoService;
import com.sxs.app.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/version")
public class AppVersionController {
    @Autowired
    AppVersionService appVersionService;
    @Autowired
    AppinfoService appinfoService;

    //根据appid查看所有版本
    @GetMapping("/list")
    public ResultVo list(Long appId){
        List<AppVersion> appVersions = appVersionService.list(appId);

        return ResultVo.success("",appVersions);
    }

    @Value("${web.upload-path}")
    private String uploadPath;

    //新增版本
    @PostMapping("/saveOrUpdate")
    public ResultVo save(AppVersion appVersion, MultipartFile file, HttpServletRequest request){
        String token = request.getHeader("token");

        if(file!=null && !file.isEmpty()){
            //前端有文件传来
            String originalFilename = file .getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if(file.getSize()>1024*500){
                return ResultVo.error("文件超过500K");
            }else if(extension.equalsIgnoreCase("apk")
            ){
                //正常上传
                String realPath = uploadPath+"upload";
                File savePath = new File(realPath);
                if(!savePath.exists()){
                    savePath.mkdirs();
                }
                //防止重名覆盖
                String fileName = UUID.randomUUID().toString().replace("-", "");
                File saveFile = new File(uploadPath+"upload/"+fileName+"."+extension);
                try {
                    file.transferTo(saveFile);
                    appVersion.setDownloadlink("/version/"+fileName+"."+extension);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件出现异常");
                }
            }else {
                return ResultVo.error("文件格式不对");
            }
        }

        DevUser devUser = (DevUser) SeesionUtil.get(token);
        //操作数据库方面的代码
        if(appVersion.getId()==null){
            appVersion.setCreatedby(devUser.getId());  //创建者
            appVersion.setCreationdate(new Date());    //当前时间点
            appVersion.setPublishstatus(3L);
            appVersionService.save(appVersion);
            //更新qpp基本信息里面的那个最新版本属性
            AppInfo appInfo = new AppInfo();
            appInfo.setId(appVersion.getAppid());
            appInfo.setVersionid(appVersion.getId());
            appinfoService.save(appInfo);

        }
        return ResultVo.success("",null);
    }
}
