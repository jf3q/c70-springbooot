package com.ithemou.admin.service;
import com.ithemou.common.dto.LoginUserDto;
import com.ithemou.common.entity.BackendUser;
import com.ithemou.common.mapper.BackendUserDao;
import com.ithemou.common.utils.SessionUtils;
import com.ithemou.common.utils.SysConstant;
import com.ithemou.common.utils.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AdminLoginService {
    @Autowired
    BackendUserDao backendUserDao;
    public LoginUserDto login(LoginUserDto userDto) {
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(userDto.getAccount());
        backendUser.setUserpassword(userDto.getPassword());
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        if (backendUsers == null || backendUsers.size() == 0) {
            throw new RuntimeException("账号或密码错误");
        }
        String token = TokenUtils.createToken(backendUsers.get(0).getId() + "", backendUsers.get(0).getUsercode(), SysConstant.UserTypeStr.admin);
        SessionUtils.saveToken(token, backendUsers.get(0));
        userDto.setToken(token);
        return userDto;

    }
}

