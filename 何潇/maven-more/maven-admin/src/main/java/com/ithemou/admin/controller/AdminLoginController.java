package com.ithemou.admin.controller;

import com.ithemou.admin.service.AdminLoginService;
import com.ithemou.common.dto.LoginUserDto;
import com.ithemou.common.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminLoginController {

    @Autowired
    AdminLoginService adminLoginService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto userDto) {
        try {
            userDto = adminLoginService.login(userDto);
            return ResultVo.success("登录成功", userDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }

    }

}
