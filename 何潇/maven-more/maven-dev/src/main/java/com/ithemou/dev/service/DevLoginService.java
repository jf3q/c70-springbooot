package com.ithemou.dev.service;

import com.ithemou.common.dto.LoginUserDto;
import com.ithemou.common.entity.DevUser;
import com.ithemou.common.mapper.DevUserDao;
import com.ithemou.common.utils.SessionUtils;
import com.ithemou.common.utils.SysConstant;
import com.ithemou.common.utils.TokenUtils;
import com.ithemou.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DevLoginService {
    @Autowired
    DevUserDao userDao;
    public LoginUserDto login(LoginUserDto userDto) {
        LoginUserVo userVo = new LoginUserVo();
        DevUser devUser = new DevUser();
        devUser.setDevcode(userDto.getAccount());
        devUser.setDevpassword(userDto.getPassword());
        List<DevUser> devUsers = userDao.queryAllBy(devUser);
        if (devUsers == null || devUsers.size() == 0) {
            throw new RuntimeException("账号密码错误");
        }
        String token = TokenUtils.createToken(devUsers.get(0).getId() + "", devUsers.get(0).getDevcode(), SysConstant.UserTypeStr.dev);
        SessionUtils.saveToken(token, devUsers.get(0));
        userDto.setToken(token);
        return userDto;
    }
}
