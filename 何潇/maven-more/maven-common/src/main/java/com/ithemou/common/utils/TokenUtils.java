package com.ithemou.common.utils;

import java.util.UUID;

public class TokenUtils {
    public static String createToken(String userId, String userCode, String userType) {

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-", ""));
        stringBuffer.append("-" + userId);
        stringBuffer.append("-" + userCode);
        stringBuffer.append("-" + userType);
        stringBuffer.append("-" + System.currentTimeMillis());
        return stringBuffer.toString();
    }


}
