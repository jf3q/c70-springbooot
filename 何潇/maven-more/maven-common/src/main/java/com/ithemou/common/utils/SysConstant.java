package com.ithemou.common.utils;

/**
 * 系统常量类
 */
public class SysConstant {
    public final static Integer pageSize=5;

    public static class UserTypeStr{
        public final static String admin="admin";
        public final static String dev="dev";
    }

    public static class UserTypeInt{
        public final static Integer admin=1;
        public final static Integer dev=2;
    }

    public static class VersionStatus{
        public final static Long no=1L;
        public final static Long yes=2L;
        public final static Long pre=3L;
    }

    public static class AppStatus{
        public final static Long pre=1L;
        public final static Long yes=2L;
        public final static Long no=3L;
        public final static Long on=4L;
        public final static Long off=5L;
    }

}
