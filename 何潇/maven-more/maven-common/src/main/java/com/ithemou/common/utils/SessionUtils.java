package com.ithemou.common.utils;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static Map<String,Object> map = new HashMap();

    //把token存起来
    public static void saveToken(String token, Object user) {
        map.put(token, user);
    }

    //校验我的token对不对

    public static Object get(String token) {
        return map.get(token);
    }

    public static void removeToken(String token){
        map.remove(token);
    }


}
