package com.ithemou.common.vo;

public class ResultVo {

    private String code;//状态码 2000 成功  5000失败
    private String message;//消息提醒
    private Object data;//携带的数据

    public ResultVo() {
    }

    public ResultVo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    //成功的
    public static ResultVo success(String message, Object data){
        return new ResultVo("2000",message,data);
    }

    //失败的
    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
