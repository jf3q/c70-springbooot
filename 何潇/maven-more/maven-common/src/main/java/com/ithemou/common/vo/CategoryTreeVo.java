package com.ithemou.common.vo;

import java.util.List;

public class CategoryTreeVo {

    private Long id;

    private String categoryname;

    private Long parentid;

    private List<CategoryTreeVo> children;

    public List<CategoryTreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTreeVo> children) {
        this.children = children;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }
}
