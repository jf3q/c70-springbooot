package com.ithemou.controller;

import com.ithemou.dto.LoginUserDto;
import com.ithemou.service.LoginUserService;
import com.ithemou.utils.SessionUtils;
import com.ithemou.vo.LoginUserVo;
import com.ithemou.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/devUser")
public class DevUserController {

    @Autowired
    LoginUserService loginUserService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto loginUserDto){
        LoginUserVo vo = null;
        try {
            vo = loginUserService.login(loginUserDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功",vo);
    }

    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("退出成功");
    }
}
