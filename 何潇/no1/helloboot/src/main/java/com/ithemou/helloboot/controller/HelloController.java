package com.ithemou.helloboot.controller;


import com.ithemou.helloboot.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    UserInfo userInfo;
    @GetMapping("/hello")
    public String test() {
        System.out.println(userInfo);
        return "hello boot!!!";
    }
}
