package com.ithemou.helloboot.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

@Data
@Component
@ConfigurationProperties(prefix = "userinfo")
public class UserInfo {
    private String name;
    private Integer age;
}
