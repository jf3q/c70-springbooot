package com.ithemou.service;

import com.ithemou.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserInfoService {

    public UserInfo getUser(Integer id) {
        return new UserInfo(1,"zhangsan","123");
    }

    public List<UserInfo> getList() {
        return new ArrayList<>(Arrays.asList(
                new UserInfo[]{
                        new UserInfo(1,"zhangsan","123"),
                        new UserInfo(2,"lis","123"),
                        new UserInfo(3,"wangw","123"),
                }
        ));
    }
}
