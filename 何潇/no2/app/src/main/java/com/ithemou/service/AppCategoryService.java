package com.ithemou.service;

import com.ithemou.dao.AppCategoryDao;
import com.ithemou.entity.AppCategory;
import com.ithemou.vo.CategoryTreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppCategoryService {

    @Autowired
    AppCategoryDao appCategoryDao;


    public CategoryTreeVo tree() {
        List<CategoryTreeVo> voList = new ArrayList<>();
        List<AppCategory> list = appCategoryDao.queryAllBy(new AppCategory());
        for (AppCategory category : list) {
            CategoryTreeVo vo = new CategoryTreeVo();
            BeanUtils.copyProperties(category,vo);
            voList.add(vo);
        }
        CategoryTreeVo tree = new CategoryTreeVo();
        for (CategoryTreeVo treeVo : voList) {
            if (treeVo.getParentid() == null){
                tree = findChildren(treeVo,voList);
            }
        }

        return tree;
    }

    private CategoryTreeVo findChildren(CategoryTreeVo vo, List<CategoryTreeVo> voList) {
        vo.setChildren(new ArrayList<>());
        for (CategoryTreeVo treeVo : voList) {
            if (treeVo.getParentid() == vo.getId()){
                vo.getChildren().add(treeVo);
                findChildren(treeVo,voList);
            }
        }
        return vo;
    }
}
