package com.ithemou;

import com.ithemou.controller.CategoryController;
import com.ithemou.dao.DevUserDao;
import com.ithemou.entity.DevUser;
import com.ithemou.service.AppCategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AppApplicationTests {
    @Autowired
    DevUserDao devUserDao;
    @Autowired
    AppCategoryService appCategoryService;
    @Autowired
    CategoryController categoryController;

    @Test
    void contextLoads() {
        System.out.println(devUserDao.queryAllBy(new DevUser()));
    }

}
