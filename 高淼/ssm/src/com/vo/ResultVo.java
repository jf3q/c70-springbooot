package com.vo;


public class ResultVo {
    private String message; //提示
    private String code;    //状态码
    private Object data;    //值


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResultVo{" +
                "message='" + message + '\'' +
                ", code='" + code + '\'' +
                ", data=" + data +
                '}';
    }

    public ResultVo(String message, String code, Object data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public ResultVo() {
    }

    //成功的
    public static ResultVo success(String message,Object data){
        return new ResultVo("成功","2000",data);
    }

    //成功的
    public static ResultVo success(){
        return new ResultVo("成功","2000",null);
    }

    //失败的
    public static ResultVo error(String message){
        return new ResultVo("失败","5000",message);
    }
}
