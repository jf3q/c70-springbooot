package com.service;

import com.dao.AppInfoDao;
import com.dao.AppVersionDao;
import com.dao.DevUserDao;
import com.entity.AppInfo;
import com.entity.DevUser;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

@Service
public class AppinfoService {
    @Autowired
    AppInfoDao appInfoDao;
    @Autowired
    AppVersionDao appVersionDao;

    @Autowired
    DevUserDao devUserDao;
    //分页
    public PageInfo<AppInfo> page(AppInfo appInfo, Integer pageNum) {
        PageHelper.startPage(pageNum,10,"id desc");
        //BeanUtils.copyProperties(appInfoDto,appInfo);copyProperties 复制
        List<AppInfo> list = appInfoDao.queryAllBy(appInfo);
        return new PageInfo<>(list);
    }

    public void save(AppInfo appInfo) {
        appInfoDao.insert(appInfo);
    }

    public void update(AppInfo appInfo) {
        appInfoDao.update(appInfo);
    }

    //删除
    public void del(Long id, HttpServletRequest request) {
//        //删除这个版本里所有的apk文件
//        AppVersion version = new AppVersion();
//        version.setAppid(id);
//        List<AppVersion> list = appVersionDao.queryAllBy(version);
//
//        for (AppVersion appVersion : list) {
//                if(appVersion.getDownloadlink() != null){
//                    String realPath = request.getServletContext().getRealPath(appVersion.getDownloadlink());
//                File saveFile = new File(realPath);
//                if(saveFile.exists()){
//                    saveFile.delete();
//                }
//            }
//            //删除所有版本
//            appVersionDao.deleteById(version.getId());
//        }
//
//        //删除app的logo文件
//        AppInfo appInfo = appInfoDao.queryById(id);
//        if(appInfo.getLogolocpath()!=null){
//            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
//            File saveFile = new File(realPath);
//            if(saveFile.exists()){
//                saveFile.delete();
//            }
//        }
//
//        //删除app
//        appInfoDao.deleteById(id);
        appInfoDao.deleteById(id);
        appVersionDao.deleteByAppId(id);
    }

    public AppInfo getBy(Long appid) {
        return appInfoDao.queryById(appid);
    }

    //通过账号获取devId(开发者id)
    public Long getDevId(String account){
        DevUser devUser = new DevUser();
        devUser.setDevcode(account);
        List<DevUser> list = devUserDao.queryAllBy(devUser);
        Long id =list.get(0).getId();
        return id;
    }

    //管理员审核
    public void shenhe(long appid, Integer ofType) {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(appid);

        if(ofType == 1){    //通过审核
            appInfo.setStatus(2L);
            appInfoDao.update(appInfo);
        }else if (ofType == 2){ //没有通过
            appInfo.setStatus(3L);
            appInfoDao.update(appInfo);
        }else {
            throw new RuntimeException("操作不合法");
        }
    }

    public Boolean validate(AppInfo appInfo) {
        if(appInfo.getId()!=null){
            //修改的操作
            AppInfo app = appInfoDao.queryById(appInfo.getId());
            if(app.getApkname().equals(appInfo.getApkname())){
                return true;
            }
        }

        //正常对比
        Integer validate = appInfoDao.validate(appInfo.getApkname());
        if(validate>0){
            return false;
        }
        return true;
    }
}
