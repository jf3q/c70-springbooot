package com.utiles;

import com.intercept.SeesionUtil;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenIntercept implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token==null){
            response.setStatus(401);
        }else if (SeesionUtil.get(token)==null){
            response.setStatus(403);
        }else {
            String[] split = token.split("-");
            if (System.currentTimeMillis()-Long.parseLong(split[2])>2*3600*1000){
                SeesionUtil.removeToken(token);
                response.setStatus(403);
            }
            return true;
        }
        return true;
    }
}
