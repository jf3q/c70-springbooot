package com.controller;

import com.service.CategoryService;
import com.vo.ResultVo;
import com.vo.TreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;
    @GetMapping("/getTree")
    public ResultVo getTree(){
        TreeVo vo =categoryService.getTree();
        return ResultVo.success("",vo);
    }
}
