package com.controller;

import com.dto.LoginDto;
import com.intercept.SeesionUtil;
import com.service.LoginService;
import com.vo.LoginUserVo;
import com.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController

//vo 一些后端传给前端的数据
//dto 前端传给后端 后端接受数据
public class LoginController {
    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginUserVo vo = null;

        try {
            vo = loginService.login(loginDto);

            return ResultVo.success("登录成功",vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }



    //注销
    @GetMapping("/logout")
    public ResultVo logout(HttpServletRequest request){
        String token = request.getHeader("token");
        //校验token
        SeesionUtil.removeToken(token);
        return ResultVo.success("退出成功",null);
    }
}
