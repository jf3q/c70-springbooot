package com.dto;

public class LoginDto {
    private String account;
    private String password;
    private Integer userType;   //1.管理员 2.开发

    public LoginDto(String account, String password, Integer userType) {
        this.account = account;
        this.password = password;
        this.userType = userType;
    }

    public LoginDto() {
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "LoginDto{" +
                "account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", userType=" + userType +
                '}';
    }
}
