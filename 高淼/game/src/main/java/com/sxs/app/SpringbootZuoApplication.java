package com.sxs.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootZuoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootZuoApplication.class, args);
    }

}