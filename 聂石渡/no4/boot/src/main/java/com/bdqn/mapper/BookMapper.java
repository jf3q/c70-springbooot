package com.bdqn.mapper;

import com.bdqn.prop.Book;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookMapper {

    @Select("select * from book")
    List<Book> selectBooks();
    void deletes(Integer[] ids);
}
