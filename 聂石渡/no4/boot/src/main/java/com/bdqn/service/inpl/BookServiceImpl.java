package com.bdqn.service.inpl;

import com.bdqn.mapper.BookMapper;
import com.bdqn.prop.Book;
import com.bdqn.service.BookService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author Administrator
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Override
    public PageInfo<Book> getPage(Integer pageNum) {
        PageHelper.startPage(pageNum,3);
        List<Book> books=bookMapper.selectBooks();
        return new PageInfo<>(books);
    }

    @Override
    public void dels(Integer[] ids) {
        bookMapper.deletes(ids);
    }
}
