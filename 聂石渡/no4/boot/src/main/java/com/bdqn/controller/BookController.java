package com.bdqn.controller;


import com.bdqn.prop.Book;
import com.bdqn.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("book")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("getPage")
    public String getPage(Model model, @RequestParam(defaultValue = "1") Integer pageNum) {
        PageInfo<Book> page = bookService.getPage(pageNum);
        model.addAttribute("page", page);
        return "index";
    }

    @PostMapping("dels")
    public String dels(Integer[] ids) {
        bookService.dels(ids);
        return "redirect:getPage";
    }
}
