package com.bdqn.service;


import com.bdqn.prop.Book;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author Administrator
 */
public interface BookService {
    PageInfo<Book> getPage(Integer pageNum);

    void dels(Integer[] ids);
}
