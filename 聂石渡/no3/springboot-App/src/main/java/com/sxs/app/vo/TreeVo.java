package com.sxs.app.vo;

import java.util.List;

public class TreeVo {
    private Long id;
    private Long parentid;  //父级节点id
    private String categoryname;    //分类名称
    private List<TreeVo> children;

    public TreeVo(Long id, Long parentid, String categoryname, List<TreeVo> children) {
        this.id = id;
        this.parentid = parentid;
        this.categoryname = categoryname;
        this.children = children;
    }

    public TreeVo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public List<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeVo> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "TreeVo{" +
                "id=" + id +
                ", parentid=" + parentid +
                ", categoryname='" + categoryname + '\'' +
                ", children=" + children +
                '}';
    }
}
