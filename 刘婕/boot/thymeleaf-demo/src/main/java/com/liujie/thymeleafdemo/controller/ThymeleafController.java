package com.liujie.thymeleafdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafController {

    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("username","zhangsan");
        return "hello";
    }
    @GetMapping("/login")
    public String login(String username,String password){
        System.out.println(username);
        System.out.println(password);
        return "hello";
    }
}
