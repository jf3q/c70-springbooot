package com.liujie.app.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liujie.app.dao.AppInfoDao;
import com.liujie.app.dto.AppInfoDto;
import com.liujie.app.entity.AppInfo;
import com.liujie.app.vo.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppInfoService {

    @Autowired
    AppInfoDao appInfoDao;

    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum) {
        PageHelper.startPage(pageNum,5,"id desc");
        AppInfo appInfo=new AppInfo();
        BeanUtils.copyProperties(appInfoDto,appInfo);
        List<AppInfo> list= appInfoDao.queryAllByLimit(appInfo);
        PageInfo pageInfo=new PageInfo(list);
        return pageInfo;
    }

    public Result getInsertOrUpdate(AppInfo appInfo) {
        if (appInfo.getId()==null){
            appInfoDao.insert(appInfo);
        }else{
            appInfoDao.update(appInfo);
        }
        return Result.success();
    }

    public int getApkName(String apkname) {
        Integer apkName = appInfoDao.getApkName(apkname);
        return apkName;
    }

    public AppInfo getSelAppInfo(Integer id) {
        AppInfo appInfo = appInfoDao.queryById(Long.valueOf(id));
        return appInfo;
    }

    public int getUpdateStatus(Integer id, String status) {
        AppInfo appInfo=new AppInfo();
        appInfo.setId(Long.valueOf(id));
        appInfo.setStatus(Long.valueOf(status));
        int update = appInfoDao.update(appInfo);
        return  update;
    }

    public int getDelete(Integer id) {
        int i = appInfoDao.deleteById(Long.valueOf(id));
        return i;
    }
}
