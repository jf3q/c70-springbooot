package com.liujie.app.controller;

import com.liujie.app.service.AppCategoryService;
import com.liujie.app.vo.Result;
import com.liujie.app.vo.TreeVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "递归查询")
public class AppCategoryController {
    @Autowired
    AppCategoryService appCategoryService;

    @GetMapping("/getTree")
    public Result getTree(){
        TreeVo vo=appCategoryService.getTree();
        return  Result.success(vo);
    }
}

