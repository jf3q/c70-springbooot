package com.liujie.app.service;

import com.liujie.app.dao.AppVersionDao;
import com.liujie.app.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppInfoVersionService {
    @Autowired
    AppVersionDao appVersionDao;
    public int getDelete(Integer id) {
        int i = appVersionDao.deleteById(Long.valueOf(id));
        return i;
    }

    public List<AppVersion> getSelVersion(Integer appid) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid(Long.valueOf(appid));
        List<AppVersion> list = appVersionDao.queryAllByLimit(appVersion);
        return list;
    }

    public int getInsert(AppVersion appVersion) {
        int insert = appVersionDao.insert(appVersion);
        return insert;
    }

    public List<AppVersion> getSelVersionYi(Integer appid) {
        List<AppVersion> appVersion = appVersionDao.selVersion(Long.valueOf(appid));
        return appVersion;
    }
}
