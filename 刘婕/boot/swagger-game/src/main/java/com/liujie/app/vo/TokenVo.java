package com.liujie.app.vo;

public class TokenVo {
    private Long id;
    private String token;
    private String account;
    private String userType;

    public TokenVo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TokenVo(String token, String account, String userType,Long id) {
        this.token = token;
        this.account = account;
        this.userType = userType;
        this.id=id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
