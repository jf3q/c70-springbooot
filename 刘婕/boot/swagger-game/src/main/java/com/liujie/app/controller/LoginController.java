package com.liujie.app.controller;

import com.liujie.app.dto.LoginDto;
import com.liujie.app.service.BackendUserService;
import com.liujie.app.service.DevUserService;
import com.liujie.app.utils.SessionUtils;
import com.liujie.app.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/user")
@Api(tags = "User操作接口")
public class LoginController {
    @Autowired
    DevUserService devUserService;
    @Autowired
    BackendUserService backendUserService;

    @PostMapping("/login")
    @ApiOperation(value="根据登录用户信息判断登录用户",notes = "根据登录用户信息判断登录用户")
    public Result login( @RequestBody LoginDto loginDto){
        if (loginDto.getType()==2){//开发者
           return devUserService.login(loginDto);
        }else if (loginDto.getType()==1){//管理者
            return backendUserService.login(loginDto);
        }else {
            return Result.error("没有此类型");
        }
    }
    @GetMapping("/loginOut")
    @ApiOperation(value="根据注销登录",notes = "根据注销登录")
    public Result loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.del(token);
        return Result.success();
    }
}
