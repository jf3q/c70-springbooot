package com.liujie.app.interceptors;

import com.liujie.app.utils.SessionUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@Component
public class InterceptorController implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token == null || token=="") {
            response.setStatus(401);
        } else if (SessionUtils.get(token) == null) {
            response.setStatus(403);
        } else {
            String[] split = token.split("-");
            if (System.currentTimeMillis() - Long.valueOf(split[4]) > 2 * 3600 * 1000) {
                response.setStatus(403);
                return false;
            }
            if (request.getRequestURI().indexOf("/appInfo/updateStatus") != -1) {
                if (split[2].equals("dev")) {
                    response.setStatus(403);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
