package com.liujie.app.service;

import com.liujie.app.dao.BackendUserDao;
import com.liujie.app.dto.LoginDto;
import com.liujie.app.entity.BackendUser;
import com.liujie.app.utils.SessionUtils;
import com.liujie.app.vo.Result;
import com.liujie.app.vo.TokenVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserService {

    @Autowired
    BackendUserDao backendUserDao;

    public Result login(LoginDto loginDto) {
        BackendUser backendUser=new BackendUser();
        backendUser.setUserpassword(loginDto.getPassword());
        backendUser.setUsercode(loginDto.getUsername());

        List<BackendUser> list = backendUserDao.queryAllByLimit(backendUser);
        if (list.size()==0){
            return Result.error("账号或密码错误！");
        }

        String token = UUID.randomUUID().toString().replace("-", "");
        token+="-"+list.get(0).getUsercode();
        token+="-admin";
        token+="-"+list.get(0).getId();
        token += "-" + System.currentTimeMillis();

        TokenVo tokenVo=new TokenVo(token,list.get(0).getUsercode(),"admin",list.get(0).getId());
        SessionUtils.put(token,tokenVo);
        return Result.success(tokenVo);
    }
}
