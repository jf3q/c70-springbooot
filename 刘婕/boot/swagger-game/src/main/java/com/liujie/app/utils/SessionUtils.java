package com.liujie.app.utils;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static Map map=new HashMap();

    public static void put(String key,Object obj){
        map.put(key,obj);
    }
    public static Object get(String key){
      return   map.get(key);
    }
    public static void  del(String key){
        map.remove(key);
    }
}
