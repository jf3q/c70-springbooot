package com.liujie.app.controller;

import com.liujie.app.dto.LoginDto;
import com.liujie.app.service.BackenduserService;
import com.liujie.app.service.DevUserService;
import com.liujie.app.utils.SessionUtils;
import com.liujie.app.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class LoginController {
    @Autowired
    DevUserService devUserService;
    @Autowired
    BackenduserService backenduserService;

    @PostMapping("/login")
    public Result login(@RequestBody LoginDto loginDto, HttpServletRequest request){
        if (loginDto.getType()==1){
            return backenduserService.getLogin(loginDto);
        }else if (loginDto.getType()==2){
            return devUserService.getLogin(loginDto);
        }else {
            return Result.error("没有此类型");
        }
    }
    @PostMapping("/loginOut")
    public Result loginOut(HttpServletRequest request){
        String token = request.getHeader("tokens");
        SessionUtils.del(token);
        return Result.success();
    }
}
