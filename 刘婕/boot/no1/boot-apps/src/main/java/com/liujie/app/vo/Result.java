package com.liujie.app.vo;

public class Result {
    private Integer code;
    private String message;
    private Object data;


    public static Result  success(Object obj){
        return  new Result(200,"",obj);
    }
    public static Result  success(){
        return  new Result(200,"",null);
    }
    public static Result error(String message){
        return  new Result(500,message,null);
    }

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Result() {
    }

}
