package com.liujie.app.service;

import com.liujie.app.dao.AppInfoDao;
import com.liujie.app.dao.AppVersionDao;
import com.liujie.app.entity.AppVersion;
import com.liujie.app.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;

   @Autowired
   AppInfoDao appInfoDao;


    public List<AppVersion> getSelVersion(Integer appid) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid(Long.valueOf(appid));
        List<AppVersion> list = appVersionDao.queryAllByLimit(appVersion);
        return list;
    }

    public int insert(AppVersion appVersion) {
        int insert = appVersionDao.insert(appVersion);
        return insert;
    }

    public Result getInsert(AppVersion appVersion) {
        int insert = appVersionDao.insert(appVersion);
        return Result.success(insert);
    }
}
