package com.liujie.app.service;

import com.liujie.app.dao.DevUserDao;
import com.liujie.app.dto.LoginDto;
import com.liujie.app.entity.DevUser;
import com.liujie.app.utils.SessionUtils;
import com.liujie.app.vo.Result;
import com.liujie.app.vo.TokenVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserService {

    @Autowired
    DevUserDao devUserDao;

    public Result getLogin(LoginDto loginDto) {
        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getUsername());
        devUser.setDevpassword(loginDto.getPassword());
        devUser.setId(loginDto.getId());

        List<DevUser> list = devUserDao.queryAllByLimit(devUser);
        if (list.size() == 0) {
            return Result.error("账号或密码错误！");
        }

        String token = UUID.randomUUID().toString().replace("-", "");
        token += "-" + list.get(0).getDevcode();
        token += "-" + list.get(0).getId();
        token += "-dev";
        token += "-" + System.currentTimeMillis();

        TokenVo tokenVo = new TokenVo(list.get(0).getId(), token, list.get(0).getDevcode(), "dev");
        SessionUtils.put(token, tokenVo);
        return Result.success(tokenVo);
    }
}
