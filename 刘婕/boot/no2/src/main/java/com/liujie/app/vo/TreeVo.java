package com.liujie.app.vo;

import java.util.List;

public class TreeVo {
    private Long id;

    private String categoryname;

    private Long parentid;

    private List<Object> children;

    public TreeVo() {
    }

    public TreeVo(Long id, String categoryname, Long parentid, List<Object> children) {
        this.id = id;
        this.categoryname = categoryname;
        this.parentid = parentid;
        this.children = children;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<Object> getChildren() {
        return children;
    }

    public void setChildren(List<Object> children) {
        this.children = children;
    }
}
