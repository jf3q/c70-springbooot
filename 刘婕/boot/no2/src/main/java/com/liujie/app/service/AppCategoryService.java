package com.liujie.app.service;

import com.liujie.app.dao.AppCategoryDao;
import com.liujie.app.entity.AppCategory;
import com.liujie.app.vo.TreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppCategoryService {

    @Autowired
    AppCategoryDao appCategoryDao;

    public TreeVo getTree() {
        List<TreeVo> vos=new ArrayList<>();

        List<AppCategory> list = appCategoryDao.queryAllByLimit(new AppCategory());
        for (AppCategory appCategory : list) {
            TreeVo vo=new TreeVo();
            BeanUtils.copyProperties(appCategory,vo);
            vos.add(vo);
        }

        TreeVo tree=new TreeVo();
        for (TreeVo vo : vos) {
            if (vo.getParentid()==null){
                tree=findTree(vo,vos);
            }
        }
        return tree;
    }

    private TreeVo findTree(TreeVo vo, List<TreeVo> vos) {
        vo.setChildren(new ArrayList<>());
        for (TreeVo treeVo : vos) {
            if (treeVo.getParentid()==vo.getId()){
                vo.getChildren().add(treeVo);
                findTree(treeVo,vos);
            }
        }
        return vo;
    }
}
