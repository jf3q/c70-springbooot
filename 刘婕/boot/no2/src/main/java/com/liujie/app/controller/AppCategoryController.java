package com.liujie.app.controller;

import com.liujie.app.service.AppCategoryService;
import com.liujie.app.vo.Result;
import com.liujie.app.vo.TreeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppCategoryController {
    @Autowired
    AppCategoryService appCategoryService;

    @GetMapping("/getTree")
    public Result getTree(){
        TreeVo treeVo=appCategoryService.getTree();
        return  Result.success(treeVo);
    }
}
