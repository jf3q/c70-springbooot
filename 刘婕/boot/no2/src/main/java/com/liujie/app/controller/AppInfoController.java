package com.liujie.app.controller;

import com.github.pagehelper.PageInfo;
import com.liujie.app.dto.AppInfoDto;
import com.liujie.app.entity.AppInfo;
import com.liujie.app.service.AppInfoService;
import com.liujie.app.utils.SessionUtils;
import com.liujie.app.vo.Result;
import com.liujie.app.vo.TokenVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/appInfo")
public class AppInfoController {
    @Autowired
    AppInfoService appInfoService;
    @Value("${web.upload-path}")
    public String uploadPath;

    @PostMapping("/page")
    public Result page(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1") Integer pageNum, HttpServletRequest request) {
        PageInfo<AppInfo> pageInfo = appInfoService.getPage(appInfoDto, pageNum);
        if (pageNum > pageInfo.getPages()) {
            pageNum = 1;
            pageInfo = appInfoService.getPage(appInfoDto, pageNum);
        }
        return Result.success(pageInfo);
    }
    @PostMapping("/insertOrUpdate")
    public Result insertOrUpdate(AppInfo appInfo, MultipartFile file,HttpServletRequest request){
        if (file!=null && !file.isEmpty()){
//            String realPath = request.getServletContext().getRealPath("upload/");
            String filename = file.getOriginalFilename();
            String extension = FilenameUtils.getExtension(filename);
            if (file.getSize()>5140000){
                return Result.error("图片大于500kb");
            }else if (extension.equalsIgnoreCase("gif")||extension.equalsIgnoreCase("png")||
                    extension.equalsIgnoreCase("jpeg")||extension.equalsIgnoreCase("jpg")){
                filename= UUID.randomUUID().toString().replace("-","")+"."+extension;
                File save=new File(uploadPath);
                if (!save.exists()){
                    save.mkdirs();
                }
                try {
                    file.transferTo(new File(uploadPath+filename+extension));
                    appInfo.setLogopicpath("/upload/"+filename);
                } catch (IOException e) {
                    e.printStackTrace();
                    return Result.error("操作失败");
                }

            }else {
                return Result.error("文件格式不正确");
            }
        }else if (appInfo.getId()==null){
            return  Result.error("文件必填");
        }
        String token = request.getHeader("tokens");
        TokenVo vo= (TokenVo) SessionUtils.get(token);
        if (appInfo.getId()==null){
            appInfo.setCreatedby(2L);
            appInfo.setStatus(1L);
            appInfo.setDevid(vo.getId());
            appInfo.setCreationdate(new Date());
        }else {
            appInfo.setStatus(1L);
            appInfo.setCreatedby(2L);
            appInfo.setCreationdate(new Date());
        }
        return appInfoService.getInsertOrUpdate(appInfo);
    }
    @PostMapping("/selApkName")
    public Result selApkName(String apkName){
        int apkName1 = appInfoService.getApkName(apkName);
        return Result.success(apkName1);
    }
    @PutMapping("/selAppInfo")
    public Result selAppInfo(Integer id){
        AppInfo appId = appInfoService.getAppId(id);
        return Result.success(appId);
    }
    @DeleteMapping("/delAppInfo")
    public Result delAppInfo(Integer id,HttpServletRequest request){
        int delAppInfo = appInfoService.getDelAppInfo(id,request);
        return Result.success(delAppInfo);
    }
    @GetMapping("/getUpdateStatus/{id}/{status}")
    public Result getUpdateStatus(@PathVariable Integer id,@PathVariable Integer status){
        int i = appInfoService.getUpdateStatus(id, status);
        return Result.success(i);
    }
}
