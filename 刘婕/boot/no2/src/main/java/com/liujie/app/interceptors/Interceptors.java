package com.liujie.app.interceptors;
import com.liujie.app.utils.SessionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Interceptors implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String tokens = request.getHeader("token");
        if (tokens == null || tokens == "") {
            response.setStatus(401);
        } else if (SessionUtils.get(tokens) == null) {
            response.setStatus(403);
        } else {
            String[] split = tokens.split("-");
            if (System.currentTimeMillis() - Long.valueOf(split[4]) > 2 * 3600 * 1000) {
                response.setStatus(403);
            }
            if (request.getRequestURI().indexOf("/appInfo/getUpdateStatus") != -1) {
                if (split[2].equals("dev")) {
                    response.setStatus(403);
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}
