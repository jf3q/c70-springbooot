package com.ye.controller;

import com.ye.service.CategoryService;
import com.ye.utils.Result;
import com.ye.vo.CategoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    CategoryService service;

    @GetMapping("/tree")
    public Result tree() {
        CategoryVo vo = service.tree();
        return Result.success("",vo);
    }
}
