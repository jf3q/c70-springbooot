package com.ye.controller;

import com.ye.dto.LoginDto;
import com.ye.service.BackendUserService;
import com.ye.service.DevUserService;
import com.ye.utils.Result;
import com.ye.utils.SessionToken;
import com.ye.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoginController {

    @Autowired
    DevUserService devUserService;
    @Autowired
    BackendUserService backendUserService;

    @GetMapping("/layOut")
    public Result out(@RequestHeader("token") String token){
        System.out.println("token = " + token);
        SessionToken.del(token);
        return Result.success("退出成功",null);
    }


    @PostMapping("/login")
    public Result login(@RequestBody LoginDto loginDto){
        try {
            if (loginDto.getUserType()==1){
                LoginUserVo vo = backendUserService.login(loginDto);
                vo.setUserType(loginDto.getUserType());
                return Result.success("登陆成功",vo);
            }else if (loginDto.getUserType()==2){
                LoginUserVo vo = devUserService.login(loginDto);
                vo.setUserType(loginDto.getUserType());
                return Result.success("登陆成功",vo);
            }else {
                return Result.error("非法请求:没有此角色");
            }
        } catch (Exception e) {
            return Result.error(e.getMessage());
        }
    }
}
