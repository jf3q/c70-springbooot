package com.ye.controller;


import com.github.pagehelper.PageInfo;
import com.ye.dto.AppInfoDto;
import com.ye.entity.AppInfo;
import com.ye.entity.DevUser;
import com.ye.service.AppInfoService;
import com.ye.utils.Result;
import com.ye.utils.SessionToken;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/appInfo")
public class AppInfoController {
    @Value("${web.upload-path}")
    String uploadPath;
    @Autowired
    AppInfoService appInfoService;

    @PostMapping("/validate")
    public Result validate(@RequestBody AppInfo appInfo){
        Boolean flag = appInfoService.validate(appInfo);
        if (flag){
            return Result.success(null, null);
        }else {
            return Result.error(null);

        }
    }

    @PutMapping("/review")
    public Result review(@RequestBody AppInfo appInfo) {
        System.out.println("appInfo = " + appInfo);
        appInfoService.saveOrUpdate(appInfo);
        return Result.success(null, null);
    }

    @PutMapping("/{id}")
    public Result sale(@PathVariable Long id) {
        try {
            appInfoService.sale(id);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("操作失败");
        }
        return Result.success(null, null);
    }

    @PostMapping("/page")
    public Result getPage(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1") Integer pageNum) {
        System.out.println("pageNum = " + pageNum);
        PageInfo<AppInfo> page = appInfoService.getPage(appInfoDto, pageNum);
        return Result.success("ok", page);
    }

    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(AppInfo appInfo, MultipartFile file, HttpServletRequest request) {
        String token = request.getHeader("token");
        String newFile = null;
        if (file != null && !file.isEmpty()) {
            String originalFilename = file.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if (file.getSize() > 5000 * 1024) {
                return Result.error("文件超过了500k");
            } else if (extension.equalsIgnoreCase("gif") || extension.equalsIgnoreCase("jpeg") ||
                    extension.equalsIgnoreCase("png") || extension.equalsIgnoreCase("jpg")) {

                File filePath = new File(uploadPath+"upload");
                if (!filePath.exists()) {
                    filePath.mkdirs();
                }
                newFile = UUID.randomUUID().toString().replace("-", "") + "." + extension;
                try {
                    file.transferTo(new File(uploadPath+"upload/" + newFile));
                    appInfo.setLogopicpath("/upload/"+newFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    return Result.error("logo图片上传失败");
                }
            } else {
                return Result.error("文件格式不对");
            }
        } else if (appInfo.getId() == null) {
            return Result.error("图片必填");
        }
        DevUser devUser = (DevUser) SessionToken.get(token);
        if (appInfo.getId() == null) {
            System.out.println("devUser.getId() = " + devUser.getId());
            appInfo.setDownloads(0L);
            appInfo.setDevid(devUser.getId());
            appInfo.setCreationdate(new Date());
            appInfo.setStatus(1L);
            appInfo.setCreatedby(devUser.getId());
        } else {
            appInfo.setModifyby(devUser.getId());
            appInfo.setModifydate(new Date());
        }
        appInfoService.saveOrUpdate(appInfo);
        return Result.success("操作成功", null);
    }

    @GetMapping("/{id}")
    public Result getOne(@PathVariable Long id) {
        AppInfo appInfo = appInfoService.getOne(id);
        return Result.success("操作成功", appInfo);
    }

    @DeleteMapping("/{id}")
    public Result del(@PathVariable Long id) {
        appInfoService.del(id);
        return Result.success("操作成功", null);
    }


}
