package com.ye.controller;

import com.ye.entity.AppInfo;
import com.ye.entity.AppVersion;
import com.ye.entity.DevUser;
import com.ye.service.AppInfoService;
import com.ye.service.VersionService;
import com.ye.utils.Result;
import com.ye.utils.SessionToken;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/version")
public class VersionController {
    @Value("${web.upload-path}")
    String uploadPath;
    @Autowired
    VersionService versionService;
    @Autowired
    AppInfoService appInfoService;
    @GetMapping("/list/{appid}")
    public Result getList(@PathVariable Long appid){
        List<AppVersion> list = versionService.getList(appid);
        return Result.success("",list);
    }


    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(AppVersion version, MultipartFile apkFile, HttpServletRequest request) {
        String token = request.getHeader("token");
        String originalFilename = apkFile.getOriginalFilename();
        String extension = FilenameUtils.getExtension(originalFilename);
        String newFile = null;
        if (!apkFile.isEmpty() && apkFile != null) {
            if (apkFile.getSize() > 500 * 1024*1024) {
                return Result.error("文件超过了500MB");
            } else if (extension.equals("apk") ) {
               ;
                File filePath = new File(uploadPath+"apk");
                if (!filePath.exists()) {
                    filePath.mkdirs();
                }
                newFile = UUID.randomUUID().toString().replace("-", "") + "." + extension;
                try {
                    apkFile.transferTo(new File(uploadPath + "apk/" + newFile));
                    version.setDownloadlink("/apk/" + newFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    return Result.error("apk上传失败");
                }
            } else {
                return Result.error("文件格式不对");
            }
        }
        DevUser devUser = (DevUser) SessionToken.get(token);
        if (version.getId() == null) {
            version.setCreatedby(devUser.getId());
            version.setPublishstatus(3L);
            version.setCreationdate(new Date());
        } else {
            version.setModifyby(devUser.getId());
            version.setModifydate(new Date());
        }
        versionService.saveOrUpdate(version);
        AppInfo appInfo = new AppInfo();
        appInfo.setId(version.getAppid());
        appInfo.setVersionid(version.getId());
        appInfoService.saveOrUpdate(appInfo);
        return Result.success("操作成功", null);
    }

}
