package com.ye.vo;

import java.util.List;

/**
 * @version 1.0
 * @Author 新中国, 万岁！
 * @Date 2023/11/13 18:13
 * @'bug退散'
 */
public class CategoryVo {
    private Long id;
    private Long parentid;
    private String categoryname;
    private List<CategoryVo> list;


    public CategoryVo() {
    }

    public CategoryVo(Long id, Long parentid, String categoryname, List<CategoryVo> list) {
        this.id = id;
        this.parentid = parentid;
        this.categoryname = categoryname;
        this.list = list;
    }

    /**
     * 获取
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取
     * @return parentid
     */
    public Long getParentid() {
        return parentid;
    }

    /**
     * 设置
     * @param parentid
     */
    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    /**
     * 获取
     * @return categoryname
     */
    public String getCategoryname() {
        return categoryname;
    }

    /**
     * 设置
     * @param categoryname
     */
    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    /**
     * 获取
     * @return list
     */
    public List<CategoryVo> getList() {
        return list;
    }

    /**
     * 设置
     * @param list
     */
    public void setList(List<CategoryVo> list) {
        this.list = list;
    }

    public String toString() {
        return "CategoryVo{id = " + id + ", parentid = " + parentid + ", categoryname = " + categoryname + ", list = " + list + "}";
    }
}
