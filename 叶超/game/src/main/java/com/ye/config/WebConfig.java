package com.ye.config;

import com.github.pagehelper.PageInterceptor;
import com.ye.interceptors.TokenInterceptors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    TokenInterceptors tokenInterceptors;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenInterceptors).addPathPatterns("/**").excludePathPatterns("/login","/upload/**","/apk/**");
    }



    @Bean
    PageInterceptor pageInterceptor(){
        return new PageInterceptor();
    }


}
