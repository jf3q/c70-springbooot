package com.ye.service;

import com.ye.dao.DevUserDao;
import com.ye.dto.LoginDto;
import com.ye.entity.DevUser;
import com.ye.utils.SessionToken;
import com.ye.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserService {

    @Autowired
    DevUserDao devUserDao;

    public LoginUserVo login(LoginDto loginDto) {
        if (StringUtils.isEmpty(loginDto.getAccount())) {
            throw new RuntimeException("账号必填");
        }

        if (StringUtils.isEmpty(loginDto.getPassword())) {
            throw new RuntimeException("密码必填");
        }
        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPassword());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if (devUsers.size() == 0) {
            throw new RuntimeException("账号密码错误");
        }
        LoginUserVo vo = new LoginUserVo();
        vo.setUserCode(devUsers.get(0).getDevcode());
        vo.setUserName(devUsers.get(0).getDevname());
        vo.setId(devUsers.get(0).getId());
        //生成token
        String token = UUID.randomUUID().toString().replace("-", "") + "-" + System.currentTimeMillis();
        vo.setToken(token);
        SessionToken.put(token, devUsers.get(0));
        return vo;
    }
}
