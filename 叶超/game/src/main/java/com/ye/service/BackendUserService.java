package com.ye.service;

import com.ye.dao.BackendUserDao;
import com.ye.dto.LoginDto;
import com.ye.entity.BackendUser;
import com.ye.utils.SessionToken;
import com.ye.vo.LoginUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserService {

    @Autowired
    BackendUserDao backendUserDao;


    public LoginUserVo login(LoginDto loginDto) {
        if (StringUtils.isEmpty(loginDto.getAccount())) {
            throw new RuntimeException("账号必填");
        }

        if (StringUtils.isEmpty(loginDto.getPassword())) {
            throw new RuntimeException("密码必填");
        }
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(loginDto.getAccount());
        backendUser.setUserpassword(loginDto.getPassword());
        BeanUtils.copyProperties(loginDto, backendUser);
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        if (backendUsers.size()==0) {
            throw new RuntimeException("账号密码错误");
        }
        LoginUserVo vo = new LoginUserVo();
        vo.setUserCode(backendUsers.get(0).getUsercode());
        vo.setUserName(backendUsers.get(0).getUsername());

        //生成token
        String token = UUID.randomUUID().toString().replace("-","")+"-"+System.currentTimeMillis()+"-"+loginDto.getUserType();
        vo.setToken(token);
        SessionToken.put(token,backendUsers.get(0));
        return vo;
    }
}
