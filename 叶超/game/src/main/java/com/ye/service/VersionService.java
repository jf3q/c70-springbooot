package com.ye.service;

import com.ye.dao.AppVersionDao;
import com.ye.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VersionService {
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> getList(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        List<AppVersion> list = appVersionDao.queryAllBy(appVersion);
        return list;
    }

    public void saveOrUpdate(AppVersion version) {
        if (version.getId() == null) {
            appVersionDao.insert(version);
        } else {
            appVersionDao.update(version);
        }

    }
}
