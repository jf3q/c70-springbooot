package com.ye.utils;




public class Result {
    private String code;
    private String msg;
    private Object data;

    public Result(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Result() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static Result success(String message, Object data) {
        return new Result("2000", message, data);
    }

//    public static Result success(String message) {
//        return new Result("2000", message,null);
//    }

    public static Result error(String message) {
        return new Result("5000", message,null);
    }
}
