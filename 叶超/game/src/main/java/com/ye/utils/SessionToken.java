package com.ye.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0
 * @Author 新中国, 万岁！
 * @Date 2023/11/11 10:56
 * @ 'Long live New China'
 */
public class SessionToken {
    private static Map<String, Object> map = new HashMap<>();

    public static void put(String key, Object obj) {
        map.put(key, obj);
    }

    public static void del(String key) {
        map.remove(key);
    }

    public static Object get(String key) {
        return map.get(key);
    }
}
