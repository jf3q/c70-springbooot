package com.ye.interceptors;

import com.ye.utils.SessionToken;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;



@Component
public class TokenInterceptors implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        String token = request.getHeader("token");
        if (token==null){
            response.setStatus(401);
        }else if (SessionToken.get(token)==null){
            response.setStatus(403);
        }else {
            String[] split = token.split("-");
            if (System.currentTimeMillis()-Long.valueOf(split[1])>2*3600*1000){
                response.setStatus(403);
                SessionToken.del(token);
            }
//            if (requestURI.contains("/review")){
//                if (Long.valueOf(split[2])==1){
//                    response.setStatus(403);
//                    return false;
//                }
//            }
            return true;
        }
        return false;
    }
}
