package com.atww.book.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/6 14:03
 * @bug退散
 */
@Configuration
@Data
@ConfigurationProperties(prefix = "book")
public class BookImg {
    private String path;
}
