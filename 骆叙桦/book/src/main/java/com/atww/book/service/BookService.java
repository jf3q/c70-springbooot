package com.atww.book.service;

import com.atww.book.entity.Book;
import com.github.pagehelper.PageInfo;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/6 10:42
 * @bug退散
 */
public interface BookService {
    PageInfo<Book> getPage(Integer pageNum, Book book);

    void addBook(Book book);

    void delMany(String[] ids);
}
