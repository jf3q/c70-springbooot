package com.atww.rabbitmq.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/9 10:13
 * @bug退散
 */
@Configuration
public class MySwaggerConfig {
    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Springboot-RabbitMQ接口文档")
                        .description("本文档用于学习SpringBoot整合RabbitMQ")
                        .version("v0.0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                        .description("RabbitMQ Wiki Documentation")
                        .url("https://springshop.wiki.github.org/docs"));
    }
}
