package com.atww.nucleic_acid.service;

import com.atww.nucleic_acid.entity.MedicalAssay;

import java.util.List;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/5 10:30
 * @bug退散
 */
public interface MedicalAssayService {

    List<MedicalAssay> getMedical(Long hId);

    void addMedical(MedicalAssay medicalAssay);

    Integer updateStatus(MedicalAssay medicalAssay);
}
