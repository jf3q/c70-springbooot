package com.atww.nucleic_acid.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2024-01-05 09:00:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hospital implements Serializable {
    private static final long serialVersionUID = -86904561631862479L;
    /**
     * 检测机构编号
     */
    private Long id;
    /**
     * 检测机构名称
     */
    private String name;




}

