package com.xjq.common.utiles;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static Map<String,Object> map=new HashMap<>();


    public static void put(String token,Object data){
        map.put(token,data);
    }

    public static Object get(String token){
        return map.get(token);
    }

    public static Object remove(String token){
        return map.remove(token);
    }
}
