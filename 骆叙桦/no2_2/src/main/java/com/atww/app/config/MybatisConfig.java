package com.sxs.app.config;

import com.github.pagehelper.PageInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 分页配置类
 */
@Configuration
public class MybatisConfig {

//<bean class="com.github.pagehelper.PageInterceptor"/>
    @Bean
    public PageInterceptor pageInterceptor(){
        return new PageInterceptor();
    }
}
