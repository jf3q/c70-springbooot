package com.sxs.app.service;

import com.sxs.app.dao.AppVersionDao;
import com.sxs.app.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> list(Long appId){
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appId);
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        return appVersions;
    }

    public void save(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }

    public void update(AppVersion appVersion) {
        appVersionDao.update(appVersion);
    }
}
