package com.sxs.app.utiles;


import com.sxs.app.intercept.SeesionUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class TokenIntercept implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token==null){
            response.setStatus(401);
        }else if (SeesionUtil.get(token)==null){
            response.setStatus(403);
        }else {
            String[] split = token.split("-");
            if (System.currentTimeMillis()-Long.parseLong(split[2])>2*3600*1000){
                SeesionUtil.removeToken(token);
                response.setStatus(403);
            }
            return true;
        }
        return true;
    }
}
