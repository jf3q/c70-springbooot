package com.sxs.app.vo;

public class LoginUserVo {
    private String account; //账号
    private String token;   //令牌
    private Long type;  //类型

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LoginUserVo{" +
                "account='" + account + '\'' +
                ", token='" + token + '\'' +
                ", type=" + type +
                '}';
    }

    public LoginUserVo(String account, String token, Long type) {
        this.account = account;
        this.token = token;
        this.type = type;
    }

    public LoginUserVo() {
    }
}
