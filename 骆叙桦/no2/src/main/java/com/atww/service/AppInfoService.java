package com.atww.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.atww.dao.AppInfoDao;
import com.atww.dao.AppVersionDao;
import com.atww.dto.ApkNameDto;
import com.atww.dto.AppInfoDto;
import com.atww.entity.AppInfo;
import com.atww.entity.AppVersion;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class AppInfoService {

    @Autowired
    AppInfoDao appInfoDao;

    @Autowired
    AppVersionDao appVersionDao;


    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNo) {
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(appInfoDto, appInfo);

        PageHelper.startPage(pageNo, 10, "id desc");
        List<AppInfo> appInfos = appInfoDao.queryAllBy(appInfo);
        return new PageInfo<>(appInfos);
    }

    public AppInfo getById(Long id) {
        return appInfoDao.queryById(id);
    }

    public void saveOrUpdate(AppInfo appInfo) {
        if (appInfo.getId() == null) {
            appInfoDao.insert(appInfo);
        } else {
            appInfoDao.update(appInfo);
        }
    }

    public void del(Long id, HttpServletRequest request) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        for (AppVersion version : appVersions) {
            if (version.getDownloadlink() != null) {
                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());
                File file = new File(realPath);
                if (file.exists()){
                    file.delete();
                }
            }
            appVersionDao.deleteById(version.getId());
        }

        AppInfo delApp = appInfoDao.queryById(id);
        if (delApp.getLogopicpath() != null){
            String realPath = request.getServletContext().getRealPath(delApp.getLogopicpath());
            File delFile = new File(realPath);
            if (delFile.exists()){
                delFile.delete();
            }
        }
        appInfoDao.deleteById(id);
    }

    public Boolean varApkName(ApkNameDto apkNameDto) {
        if (apkNameDto.getId() != null){
            AppInfo appInfo = appInfoDao.queryById(apkNameDto.getId());
            if (apkNameDto.getApkName().equals(appInfo.getApkname())){
                return true;
            }
        }

        AppInfo appInfo = new AppInfo();
        appInfo.setApkname(apkNameDto.getApkName());
        List<AppInfo> appInfos = appInfoDao.queryAllBy(appInfo);
        if (appInfos.size()>0){
            return false;
        }
        return true;
    }


    public void sale(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);
        if (appInfo.getStatus() == 5L){
            appInfo.setStatus(4L);
            appInfo.setOnsaledate(new Date());
        }else if (appInfo.getStatus() == 4L){
            appInfo.setStatus(5L);
            appInfo.setOffsaledate(new Date());
        }else if (appInfo.getStatus() == 2L){
            appInfo.setStatus(4L);
            appInfo.setOffsaledate(new Date());
        }
        appInfoDao.update(appInfo);
    }
}
