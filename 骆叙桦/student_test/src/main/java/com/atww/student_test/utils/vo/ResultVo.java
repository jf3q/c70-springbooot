package com.atww.student_test.utils.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 14:55
 * @bug退散
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private Boolean flag;
    private String message;
    private Object data;

    public static ResultVo success(String message,Object data){
        return new ResultVo(true,message,data);
    }
    public static ResultVo error(String message){
        return new ResultVo(false,message,null);
    }
}
