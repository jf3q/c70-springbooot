package com.atww.student_test.serivce;

import com.atww.student_test.pojo.Student;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 14:58
 * @bug退散
 */
public interface StudentService extends IService<Student> {

}
