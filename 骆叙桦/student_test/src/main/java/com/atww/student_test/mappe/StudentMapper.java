package com.atww.student_test.mappe;

import com.atww.student_test.pojo.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 13:29
 * @bug退散
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
