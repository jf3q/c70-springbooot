package com.atww.student_test.controller;

import com.atww.student_test.mappe.StudentMapper;
import com.atww.student_test.pojo.Student;
import com.atww.student_test.serivce.StudentService;
import com.atww.student_test.utils.vo.ResultVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 14:54
 * @bug退散
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("")
    public ResultVo getPageByName(String name,@RequestParam(defaultValue = "1") Integer pageNum){
        LambdaQueryWrapper<Student> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.hasText(name),Student::getStudentname,name);
        queryWrapper.orderByDesc(Student::getId);
        Page<Student> page=new Page<>(pageNum,3);
        return ResultVo.success("分页成功",studentService.page(page,queryWrapper));
    }

    @PostMapping("")
    public ResultVo saveOrUpdate(@RequestBody Student student){
        try {
            studentService.saveOrUpdate(student);
            return ResultVo.success("操作成功",null);
        }catch (Exception e){
            return ResultVo.error(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResultVo delete(@PathVariable Integer id){
        try {
            studentService.removeById(id);
            return ResultVo.success("操作成功",null);
        }catch (Exception e){
            return ResultVo.error(e.getMessage());
        }
    }
}
