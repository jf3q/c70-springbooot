package com.atww.student_test.serivce.impl;

import com.atww.student_test.mappe.StudentMapper;
import com.atww.student_test.pojo.Student;
import com.atww.student_test.serivce.StudentService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 14:58
 * @bug退散
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
}
