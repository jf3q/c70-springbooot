package com.atww.student_test.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 汪汪
 * @version 1.0
 * @data 2024/1/10 13:30
 * @bug退散
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String studentname;
    private String gender;
    private Integer age;
    private String address;

}
